// category-arasayfa listView & Hoverimg _ Kitaplar Websiteleri

var currentRequest = null; 

$(document).ready(function() {
    ilkbaslarken();
  });
  
  offsets = {'Kitaplar':0, 'Web_Siteleri':0}
  function ilkbaslarken(){
    baslarken();
  }
  function baslarken(){
  
  // Kitaplar listView
  // Kitaplar listView
  $(".listSwitch").toggle(function(){
        $(this).addClass("listview");
        $(this).siblings("ul.hoverimg").fadeOut("fast", function() {
            $(this).fadeIn("fast").removeClass("hoverimg");
            $(this).fadeIn("fast").addClass("booktoc");
       });
    }, function () {
        $(this).removeClass("listview");
        $(this).siblings("ul").fadeOut("fast", function() {
            $(this).fadeIn("fast").removeClass("booktoc");
            $(this).fadeIn("fast").addClass("hoverimg");
        });
    });
/*
  // Web_Siteleri listView
  $("#Web_Siteleri .listSwitch").toggle(function(){
        $(this).addClass("listview");
        $("#Web_Siteleri ul.hoverimg").fadeOut("fast", function() {
            $(this).fadeIn("fast").removeClass("hoverimg");
            $(this).fadeIn("fast").addClass("booktoc");
       });
    }, function () {
        $(this).removeClass("listview");
        $("#Web_Siteleri ul").fadeOut("fast", function() {
            $(this).fadeIn("fast").removeClass("booktoc");
            $(this).fadeIn("fast").addClass("hoverimg");
        });
    });*/
    $("#Kitaplar ul.hoverimg li").unbind('hover').hover(function(){
    $(this).find("img").animate({left:"-75px"},{queue:false, duration:200});
    }, function(){
    $(this).find("img").animate({left:"0px"},{queue:false, duration:200});
    });
    
    $("#Web_Siteleri ul.hoverimg li").unbind('hover').hover(function(){
    $(this).find("img").animate({top:"-85px"},{queue:false, duration:300});
    }, function(){
    $(this).find("img").animate({top:"0px"},{queue:false, duration:300});
    });
    
    $('#Kitaplar_next').unbind('click').click(function(){     listePlaces('Kitaplar',    +1); return false;});
    $('#Web_Siteleri_next').unbind('click').click(function(){ listePlaces('Web_Siteleri',+1); return false;});
    $('#Kitaplar_prev').unbind('click').click(function(){     listePlaces('Kitaplar',    -1); return false;});
    $('#Web_Siteleri_prev').unbind('click').click(function(){ listePlaces('Web_Siteleri',-1); return false;});

    
    $(".prev a").unbind('click').click(function(e){
    	getWebService(this,-1);
    });
    
    $(".next a").unbind('click').click(function(e){
    	getWebService(this,1);
    });
    
    $("#searchField").keyup(function () {
		var filter = $(this).val();
		if(filter.length >2) {
			$("#searchField").css("background" ,"white url(/assets/images/loading-gray.gif) right no-repeat");
			getWebService(this,0);
		}
    });
    
  }
  
  function getWebService(element,type){
	  var parent_id; 
	  $(element).parents().each(function(){
		  if($(this).hasClass("banner")){
			  parent_id = $(this).attr("id");
		  }
	  });
	  var parent = parent_id;
	  id = parent.split("98789");
	  id = id[1];
	  var offset ;
	  if($("#"+parent_id+"[offset]").length){
		  offset = $("#"+parent_id).attr("offset");
		  var total = $("#"+parent_id).attr("total");
		  var limit = $("#"+parent_id).attr("limit");
		  var searchField = $("#"+parent_id).find("#searchField").val();
		  
		  if(searchField === undefined || searchField === null) {
			  searchField = "";
		  }
		  
		  offset = ( parseInt(offset)+parseInt(limit) <= parseInt(total) ) ? offset : type==1 ? total : parseInt(offset) ;
		  var last = ( parseInt(offset)+parseInt(limit) >= parseInt(total) ) ? 1 : 0 ; 
	  }else{
		  offset = 0;
	  }
	 
	  if(type == 0 ){
		  offset = 0;
	  }
	  
	  var url = "/ajax/webservice-pagination/id/"+id+"/offset/"+offset+"/type/"+type+"/total/"+total+"/last/"+last+"/searchField/"+searchField;
	  console.log(url);
	  if(type==-1 && offset==0){
		  
	  }else{
		
	  currentRequest = $.ajax({
	      type: 'POST',
	      data:"banner_html_id="+parent_id,
	      url: url,
	      beforeSend : function()    {           
	          if(currentRequest != null) {
	              currentRequest.abort();
	          }
	      },
	      success: function(response) {
	    	  $("#searchField").css("background" ,"");
			  $("#"+parent_id).html(response);
		      baslarken();
	      },
	      error:function(e){
	        // Error
	      }
	  });
	  
	  }
  }
  
    function listePlaces(what, dir){
      var offset = offsets[what]+dir;
      if(offset < 0)
        return;
      var url ='/category/pagination/type/'+what+'/offset/'+offset+'/cat/'+catid;
      offsets[what] = offset;
      $('#'+what).attr('offset',offset);
      $.get(url,function(data){
        $('#'+what).replaceWith(data);
        //html(data);
        baslarken();
      });
    }
/*
$(document).ready(function(){
$('#fosilYatagi').load('/webservice/fosil');
})*/