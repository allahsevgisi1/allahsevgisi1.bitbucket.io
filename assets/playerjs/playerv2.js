﻿

var default_settings = {

    preload: 'auto',
    autostart: false ,
    autonext: false ,
    autodelay: 10,
    volume: 0.5,
    /*vod: {
     playlist: "playlist.json",
     related : "related.json",
     },*/
    /*
     live: {
     streams: [
     {
     language : "en",
     language_name: "English",
     lang_code: 4,
     order: 2,
     streamurl: "" //m3u8
     },
     {
     language : "tr",
     language_name: "Türkçe",
     lang_code: 1,
     order: 1,
     streamurl: "" //m3u8
     }
     ],
     related: {
     file: '/index/related.videos?objectId=234170',
     onclick: 'play'
     }
     }
     */

}

function Player(el,def){


    var self = this;

    function makeid(){
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for(var i=0; i < 30; i++)
        {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }


    function lsTest(){
        var test = 'test';
        try {
            localStorage.setItem(test, test);
            localStorage.removeItem(test);
            return true;
        } catch(e) {
            return false;
        }
    }

    if(lsTest() === true){
        self.player_unique_id = makeid();
        try {

            if(localStorage.getItem('sessionid') !== null){
                self.player_unique_id = localStorage.getItem('sessionid');
            }else{
                localStorage.setItem("sessionid", self.player_unique_id);
            }
        } catch(e) {

        }
    }else{
        self.player_unique_id = makeid();
    }

    console.log(self.player_unique_id);



    this.ayarlar = def;

    this.videocontainer = undefined;

    this.aktifenboyorani = 16 / 9;



    this.elementOlustur = function(parent,tag,classlar){

        var yeniElement = document.createElement(tag);
        for (var i = 0; i < classlar.length; i++) {
            yeniElement.classList.add(classlar[i]);
        }
        return parent.appendChild( yeniElement );

    };

    if (this.ayarlar === undefined) this.ayarlar = default_settings;

    console.log(this.ayarlar);


    if (typeof el == "string"){
        this.videocontainer = document.querySelector(el);
    }

    this.videocontainer.innerHTML = "";
    console.log(this.videocontainer);

    this.videoelement = this.elementOlustur(this.videocontainer, "video", ["canli-yayin-player"] );

    var sesdeger = 0.5;

    try{

        console.log( localStorage.getItem('sesdeger'));

        sesdeger = localStorage.getItem('sesdeger') || 0.5;
    }catch(e){

    }

    console.log("ses: " + this.ayarlar.volume || sesdeger);

    this.videoelement.volume = this.ayarlar.volume || sesdeger;

    this.videocontainer.style.backgroundColor = "black";

    this.playerOverlay = this.elementOlustur(this.videocontainer, "div", ["canli-yayin-player-osd"] );



    this.ilgilivideolarOverlay = this.elementOlustur(this.videocontainer, "div", ["canli-yayin-ilgili-videolar"] );

    //this.ilgilivideolarOverlay.style.display = "none";

    this.ilgilivideolarustContainer = this.elementOlustur(this.ilgilivideolarOverlay, "div", ["beyazmetin"] );
    this.ilgilivideolarustContainer.innerHTML = "İlgili Videolar";
    //this.ilgilivideolarustContainer.style.display = "none";

    this.ilgilivideolarKapatDugmesi = this.elementOlustur(this.ilgilivideolarOverlay, "button", ["kapatdugmesi"] );
    this.ilgilivideolarKapatDugmesi.innerHTML = "<img src='/assets/playerjs/kapat.png'/>";


    this.ilgilivideolarContainer = this.elementOlustur(this.ilgilivideolarustContainer, "div", ["beyazmetin"] );

    this.ilgilivideolarustContainer.style.height = "100%";
    this.ilgilivideolarContainer.style.height = "100%";

    self.ilgilivideolarOverlay.style.display = "none";

    this.ilgilivideolarKapatDugmesi.onclick = function(e){
        self.ilgilivideolarOverlay.style.display = "none";
        e.stopPropagation();
    };



    this.ilgilivideolarButton = this.elementOlustur(this.playerOverlay, "div", ["ilgili-videolar-dugmesi"] );
    this.ilgilivideolarButton.innerHTML = "<img style='vertical-align: middle;' src='/assets/playerjs/list.png'><span style='margin-left: 5px;margin-right: 3px; vertical-align: middle;'>İlgili Videolar</span>";
    this.ilgilivideolarButton.style.cursor = "pointer";
    this.ilgilivideolarButton.onclick = function(e){
        self.ilgilivideolarOverlay.style.display = "block";

        if (self.videoelement.paused){

        }else{

            setTimeout(function () {
                if (!self.videoelement.paused) {
                    self.videoelement.pause();
                }
            }, 150);

        }

        e.stopPropagation();

    };

    this.gradientAlan = this.elementOlustur(this.playerOverlay, "div", ["canli-yayin-player-oynatim-gradient"] );

    this.gradientAlan.onclick = function(e){
        e.stopPropagation();
    };


    this.ayarlarBackgroundAlani = this.elementOlustur(this.playerOverlay, "div", ["canli-yayin-player-settings-menu"] );
    this.ayarlarBackgroundAlani.style.display = "none";
    this.ayarlarBackgroundAlani.innerText = "";

    this.kaliteAyarlari = this.elementOlustur(this.playerOverlay, "div", ["canli-yayin-player-kalite-menu"] );
    this.kaliteAyarlari.style.display = 'none';
    this.kaliteAyarlari.innerText = " ";
    //canli-yayin-player-settings-menu-onceki  canli-yayin-player-settings-menu-aktif


    this.oynatimAlani = this.elementOlustur(this.playerOverlay, "div", ["canli-yayin-player-oynatim-alt-menu"] );

    this.oynatimAlani.onclick = function(e){
        e.stopPropagation();
    };

    this.oynatimAlaniSolDugmeler = this.elementOlustur(this.oynatimAlani, "div", ["canli-yayin-player-oynatim-alt-menu-soldaki-dugmeler"] );
    this.oynatimAlaniSagDugmeler = this.elementOlustur(this.oynatimAlani, "div", ["canli-yayin-player-oynatim-alt-menu-sagdaki-dugmeler"] );

    this.playDugmesi = this.elementOlustur(this.oynatimAlaniSolDugmeler, "div", ["player-dugme" , "canli-yayin-play-dugmesi"] );


    this.playdugmesiSvg = '<svg height="100%" version="1.1" viewBox="0 0 36 36" width="100%">' +
        '<use class="ytp-svg-shadow" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ytp-svg-26"></use>' +
        '<path class="ytp-svg-fill" d="M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z" id="ytp-svg-26"></path>' +
        '</svg>';


    this.playDugmesi.innerHTML = this.playdugmesiSvg;

    this.volumespan = this.elementOlustur(this.oynatimAlaniSolDugmeler, "span", [] );

    this.volumeSVG = this.elementOlustur(this.volumespan, "button", ["player-dugme","canli-yayin-ses-dugmesi"] );

    this.volumedugmesiSvg = '<svg height="100%" version="1.1" viewBox="0 0 36 36" width="100%">' +
        '<use class="ytp-svg-shadow" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ytp-svg-13"></use>' +
        '<path class="ytp-svg-fill ytp-svg-fillses" d="M8,21 L12,21 L17,26 L17,10 L12,15 L8,15 L8,21 Z M19,14 L19,22 C20.48,21.32 21.5,19.77 21.5,18 C21.5,16.26 20.48,14.74 19,14 Z M19,11.29 C21.89,12.15 24,14.83 24,18 C24,21.17 21.89,23.85 19,24.71 L19,26.77 C23.01,25.86 26,22.28 26,18 C26,13.72 23.01,10.14 19,9.23 L19,11.29 Z" id="ytp-svg-13"></path>' +
        '<svg class="ytp-svg-sound-mute-group" style="opacity: 0;">' +
        '<use class="ytp-svg-shadow" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ytp-svg-14"></use>' +
        '<path class="ytp-svg-fill" d="M 26.11,15.73 24.85,14.5 22.52,16.76 20.20,14.5 18.94,15.73 21.26,18 18.94,20.26 20.20,21.5 22.52,19.23 24.85,21.5 26.11,20.26 23.79,18 l 2.32,-2.26 0,0 z" id="ytp-svg-14"></path>' +
        '</svg>' +
        '</svg>';

    this.volumeSVG.innerHTML = this.volumedugmesiSvg;

    self.oncekises = 0.5;
    this.volumeSVG.onclick = function(e){
        if (self.sesAyari.value > 0){
            self.oncekises = self.sesAyari.value;
            self.sesAyari.value = 0;
        }else{
            self.sesAyari.value = self.oncekises;
        }
        sesAyari();
        e.stopPropagation();

    };



    self.videocontainer_fade_anim_timer;

    var oncekix = 0;
    var oncekiy = 0;

    function mouseoverVideo(e) {

        if (oncekix == e.clientX && oncekiy == e.clientY) return;
        //console.log("mouseover");

        clearInterval(self.videocontainer_fade_anim_timer);
        self.OSDGoster ();
        self.videocontainer_fade_anim_timer = setTimeout( self.OSDGizle , 2750 );

        oncekix = e.clientX;
        oncekiy = e.clientY;

    }

    this.videocontainer.addEventListener ("mousemove", mouseoverVideo);
    this.playerOverlay.addEventListener  ("mousemove", mouseoverVideo);

    self.playerOverlay.onclick = function(e){
        console.log("videoelement click");
        playpause();
        e.stopPropagation();
    };

    function playpause(){
        if (self.videoelement.paused){
            setTimeout(function () {
                if (self.videoelement.paused) {
                    self.videoelement.play();
                }
            }, 50);
        }else{

            setTimeout(function () {
                if (!self.videoelement.paused) {
                    self.videoelement.pause();
                }
            }, 50);
        }
    };

    this.mountolmusmu = function(){
        return self.hls.mounted;
    }
    this.playlive = function(){

        console.log("yayinmodu" , self.yayinmodu);

        if (self.yayinmodu === "live" || self.yayinmodu === "m3u8_on_demand"  ){


            if (self.hls !== undefined && self.hls.mounted === false){

                self.hls.loadSource(self.hls.hedefurl);
                self.hls.attachMedia(self.videoelement);

                self.hls.mounted = true;

            }

        }


        if (self.videoelement.paused){
            setTimeout(function () {
                if (self.videoelement.paused) {
                    self.videoelement.play();
                }
            }, 50);
        }

    }

    function pause_live(){
        if (!self.videoelement.paused){

            setTimeout(function () {
                if (!self.videoelement.paused) {
                    self.videoelement.pause();
                }
            }, 50);

        }
    }


    self.videoelement.onclick = function(e){
        console.log("videoelement click");
        if (self.videoelement.paused){
            setTimeout(function () {
                if (self.videoelement.paused) {
                    self.videoelement.play();
                }
            }, 50);
        }else{

            setTimeout(function () {
                if (!self.videoelement.paused) {
                    self.videoelement.pause();
                }
            }, 50);
        }
        e.stopPropagation();
    }

    var mouseleaveanimasyonuaktif = false;
    function mouseleaveVideo(){

        console.log("mouseleave func");


        clearInterval(self.videocontainer_fade_anim_timer);
        self.videocontainer_fade_anim_timer = setTimeout( self.OSDGizle , 1500 );


    }

    this.videocontainer.addEventListener ("mouseleave", mouseleaveVideo);
    this.playerOverlay.addEventListener  ("mouseleave", mouseleaveVideo);

    this.OSDGizle = function(){

        if (mouseleaveanimasyonuaktif == true) return;
        mouseleaveanimasyonuaktif = true;

        FX.fadeOut( self.playerOverlay , {
            duration: 700,
            complete: function() {
                mouseleaveanimasyonuaktif = false;
                self.playerOverlay.style.display = "none";
            }
        });

    }


    var gosterimsurecinde = false;
    this.OSDGoster = function(){

        if (gosterimsurecinde == true) return;

        if (self.playerOverlay.style.display == "none"){
            gosterimsurecinde = true;
            self.playerOverlay.style.opacity = 0;
            self.playerOverlay.style.display = "block";
            FX.fadeIn( self.playerOverlay , {
                duration: 150,
                complete: function() {
                    gosterimsurecinde = false;
                }
            });

        }

    }



    this.sesAyarAlani = this.elementOlustur(this.oynatimAlaniSolDugmeler, "div", ["canli-yayin-ses-ayar-alani"] );
    this.sesAyari = this.elementOlustur(this.sesAyarAlani, "input", ["canli-yayin-ses-ayari"] );

    this.sesAyari.setAttribute("type", "range");
    this.sesAyari.min = 0;
    this.sesAyari.max = 100;

    console.log(this.videoelement.volume);

    this.sesAyari.value = this.videoelement.volume * 100;

    this.yayinMetinAlani = this.elementOlustur(this.oynatimAlaniSolDugmeler, "div", ["canli-yayin-bilgi-alani"] );
    this.yayinMetinAlani.style.width = '200px';
    this.yayinMetinAlani.innerHTML = '';

    this.izleyicisayisigosterimalani = this.elementOlustur(this.oynatimAlaniSagDugmeler, "div", ["canli-yayin-izleyenler-alani", "noselect"] );
    this.izleyicisayisigosterimalani.innerHTML =  '';
    this.izleyicisayisigosterimalani.setAttribute("alt" , "Canlı izleyici sayısı");
    this.izleyicisayisigosterimalani.setAttribute("title" , "Canlı izleyici sayısı");

    this.kaliteDugmesi = this.elementOlustur(this.oynatimAlaniSagDugmeler, "div", ["canli-yayin-ayarlar-alani"] );
    this.kaliteDugmesi.innerHTML =  '<img style="margin-top: 7px;" src="/assets/playerjs/hd.png" width="24" height="24"/>';

    this.kaliteDugmesi.onclick=function(e){

        try{
            if (self.yayinmodu !== "live") {
                self.videothumbcontainer.className = "video-thumb-container";
            }
        }catch(e){}


        self.kalitemenusuGosterGizle();
        e.stopPropagation();
    };


    this.sesAyari.onclick = function(e){
        e.stopPropagation();
    }








    /*'<svg height="100%" version="1.1" viewBox="0 0 36 36" width="100%">' +
     '<use class="ytp-svg-shadow" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ytp-svg-16"></use>' +
     '<path d="m 23.94,18.78 c .03,-0.25 .05,-0.51 .05,-0.78 0,-0.27 -0.02,-0.52 -0.05,-0.78 l 1.68,-1.32 c .15,-0.12 .19,-0.33 .09,-0.51 l -1.6,-2.76 c -0.09,-0.17 -0.31,-0.24 -0.48,-0.17 l -1.99,.8 c -0.41,-0.32 -0.86,-0.58 -1.35,-0.78 l -0.30,-2.12 c -0.02,-0.19 -0.19,-0.33 -0.39,-0.33 l -3.2,0 c -0.2,0 -0.36,.14 -0.39,.33 l -0.30,2.12 c -0.48,.2 -0.93,.47 -1.35,.78 l -1.99,-0.8 c -0.18,-0.07 -0.39,0 -0.48,.17 l -1.6,2.76 c -0.10,.17 -0.05,.39 .09,.51 l 1.68,1.32 c -0.03,.25 -0.05,.52 -0.05,.78 0,.26 .02,.52 .05,.78 l -1.68,1.32 c -0.15,.12 -0.19,.33 -0.09,.51 l 1.6,2.76 c .09,.17 .31,.24 .48,.17 l 1.99,-0.8 c .41,.32 .86,.58 1.35,.78 l .30,2.12 c .02,.19 .19,.33 .39,.33 l 3.2,0 c .2,0 .36,-0.14 .39,-0.33 l .30,-2.12 c .48,-0.2 .93,-0.47 1.35,-0.78 l 1.99,.8 c .18,.07 .39,0 .48,-0.17 l 1.6,-2.76 c .09,-0.17 .05,-0.39 -0.09,-0.51 l -1.68,-1.32 0,0 z m -5.94,2.01 c -1.54,0 -2.8,-1.25 -2.8,-2.8 0,-1.54 1.25,-2.8 2.8,-2.8 1.54,0 2.8,1.25 2.8,2.8 0,1.54 -1.25,2.8 -2.8,2.8 l 0,0 z" fill="#fff" id="ytp-svg-16"></path>' +
     '</svg>';*/


    //this.ayarlarDugmesi.innerHTML = this.ayarlardugmesisvgmetin;

    this.tamekranDugmesi = this.elementOlustur(this.oynatimAlaniSagDugmeler, "div", ["player-dugme","canli-yayin-tam-ekran-dugmesi"] );
    this.tamekrandugmesisvgmetin = '<svg height="100%" version="1.1" viewBox="0 0 36 36" width="100%">' +
        '<g class="ytp-fullscreen-button-corner-0">' +
        ' <use class="ytp-svg-shadow" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ytp-svg-21"></use>' +
        ' <path class="ytp-svg-fill" d="m 10,16 2,0 0,-4 4,0 0,-2 L 10,10 l 0,6 0,0 z" id="ytp-svg-21"></path>' +
        '</g>' +
        '<g class="ytp-fullscreen-button-corner-1">' +
        ' <use class="ytp-svg-shadow" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ytp-svg-22"></use>' +
        ' <path class="ytp-svg-fill" d="m 20,10 0,2 4,0 0,4 2,0 L 26,10 l -6,0 0,0 z" id="ytp-svg-22"></path>' +
        '</g>' +
        '<g class="ytp-fullscreen-button-corner-2">' +
        ' <use class="ytp-svg-shadow" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ytp-svg-23"></use>' +
        ' <path class="ytp-svg-fill" d="m 24,24 -4,0 0,2 L 26,26 l 0,-6 -2,0 0,4 0,0 z" id="ytp-svg-23"></path>' +
        '</g>' +
        '<g class="ytp-fullscreen-button-corner-3">' +
        ' <use class="ytp-svg-shadow" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ytp-svg-24"></use>' +
        ' <path class="ytp-svg-fill" d="M 12,20 10,20 10,26 l 6,0 0,-2 -4,0 0,-4 0,0 z" id="ytp-svg-24"></path>' +
        '</g>' +
        '</svg>';

    var isFull = false;
    self.videocontainer.addEventListener('webkitfullscreenchange', function(e) {

        isFull = !isFull;

        console.log("webkitfullscreenchange");

    });

    this.tamekranDugmesi.onclick = function(e){
        var i = self.videocontainer;

        console.log("tam ekran değişimi: " + isFull);
        if (!isFull){

            if (i.requestFullscreen) {
                i.requestFullscreen();
            } else if (i.webkitRequestFullscreen) {
                i.webkitRequestFullscreen();
            } else if (i.mozRequestFullScreen) {
                i.mozRequestFullScreen();
            } else if (i.msRequestFullscreen) {
                i.msRequestFullscreen();
            }

        }else{

            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }

        }
        e.stopPropagation();

    }

    this.tamekranDugmesi.innerHTML = this.tamekrandugmesisvgmetin;


    this.sliderContainer = this.elementOlustur( this.oynatimAlani , "div", ["player-konum"] );
    this.sliderContainer.style.display = "none";

    this.slider = this.elementOlustur( this.sliderContainer , "input", [] );
    this.slider.min = "1";
    this.slider.max = "10000";
    this.slider.style.width = "100%";
    this.slider.setAttribute("type","range");



    this.slideradokunma = false;
    this.slidertimer;

    this.slider.oninput = function(){

        self.slideradokunma = true;

        if (self.slidertimer !== undefined){
            clearInterval(self.slidertimer);
        }

        self.slidertimer = setTimeout(function(){
            self.slideradokunma = false;
        },2000)

    };

    this.slider.onclick= function(e){
        e.stopPropagation();
    };
    this.sliderContainer.onclick= function(e){
        e.stopPropagation();
    };

    this.thumbgizletimer = undefined;

    this.slider.onmousemove = function(e){

        if (self.yayinmodu !== "live" && self.ayarlar.thumbdurum === 1 ){

            var x = e.offsetX;
            var w = this.offsetWidth;

            var onizleme_resim_sayisi  = ((x / w) * 640);

            onizleme_resim_sayisi = Math.max(1, onizleme_resim_sayisi);
            onizleme_resim_sayisi = Math.min(640, onizleme_resim_sayisi);
            onizleme_resim_sayisi = Math.floor( onizleme_resim_sayisi);

            var thumb_image_id = Math.ceil(onizleme_resim_sayisi / 16);

            var icerik1 = onizleme_resim_sayisi - ( (Math.ceil(onizleme_resim_sayisi / 16) - 1) * 16 );

            var thumb_x_katman = icerik1 % 4;
            var thumb_y_katman = Math.floor(icerik1 / 4);

            var link = "//g.fmanager.net/files/video/vthumb/" + self.ayarlar.thumbid + "/" + thumb_image_id + ".jpg";

            //console.log(link);
            self.thumbalan.style.backgroundImage = "url(" + link + ")";

            self.thumbalan.style.backgroundPosition = -((thumb_x_katman * 160)) + "px " +  -((thumb_y_katman * 90))  + "px";

            self.videothumbcontainer.style.position = "absolute";
            self.videothumbcontainer.style.bottom = "75px";

            var left = (24 + e.offsetX) - (166 / 2);
            left = Math.min( 24 + w + 24 - ( 166 +25 ) , left);
            left = Math.max( 25 , left);

            self.videothumbcontainer.style.left = left + "px";
            self.videothumbcontainer.style.display = "block";

            self.videothumbcontainer.className = "video-thumb-container video-thumb-container-gorunur";

            self.kalitemenusuGizle();

            clearTimeout(self.thumbgizletimer);
            self.thumbgizletimer = setTimeout( function() {
                self.videothumbcontainer.className = "video-thumb-container";
            }, 1500 );

            //console.log(x, w , onizleme_resim_sayisi, thumb_image_id, icerik1, thumb_x_katman , thumb_y_katman,  (thumb_x_katman * 80) + "px " +  (thumb_y_katman * 45)  + "px");
            //console.log( thumb_image_id + ".jpg" ,  self.thumbalan.style.backgroundPosition );

        }


    };
    this.slider.onchange = function(){
        console.log(self.slider.value);
        console.log(self.videoelement.duration);
        console.log(self.videoelement.currentTime);
        if (self.yayinmodu == "live"){
            self.slider.style.display = "none";
            return;
        }

        if (self.videoelement.duration === undefined || self.videoelement.currentTime === undefined || isNaN(self.videoelement.duration) || isNaN(self.videoelement.currentTime) ){
            self.slider.value = 0;
            return;
        }

        console.log(self.slider.value);

        self.videoelement.currentTime = (self.slider.value / 10000) * self.videoelement.duration;

    }

    self.sesAyari.value = self.videoelement.volume;

    this.sesAyari.onchange = function(){

        sesAyari();

    }

    this.sesAyari.oninput = function(){

        sesAyari();

    }

    function sesAyari(){


        self.videoelement.volume = self.sesAyari.value / 100;

        try{
            localStorage.setItem('sesdeger', self.videoelement.volume );
        }catch(e){ }


        //"ytp-svg-fillses";
        if (self.videoelement.volume > 0.5){
            self.volumeSVG.querySelector(".ytp-svg-fillses").setAttribute("d","M8,21 L12,21 L17,26 L17,10 L12,15 L8,15 L8,21 Z M19,14 L19,22 C20.48,21.32 21.5,19.77 21.5,18 C21.5,16.26 20.48,14.74 19,14 Z M19,11.29 C21.89,12.15 24,14.83 24,18 C24,21.17 21.89,23.85 19,24.71 L19,26.77 C23.01,25.86 26,22.28 26,18 C26,13.72 23.01,10.14 19,9.23 L19,11.29 Z");
            self.volumeSVG.querySelector(".ytp-svg-sound-mute-group").style.opacity = 0;

        }else if (self.videoelement.volume > 0){
            self.volumeSVG.querySelector(".ytp-svg-fillses").setAttribute("d","M8,21 L12,21 L17,26 L17,10 L12,15 L8,15 L8,21 Z M19,14 L19,22 C20.48,21.32 21.5,19.77 21.5,18 C21.5,16.26 20.48,14.74 19,14 Z");
            self.volumeSVG.querySelector(".ytp-svg-sound-mute-group").style.opacity = 0;

        }else{
            self.volumeSVG.querySelector(".ytp-svg-fillses").setAttribute("d","M8,21 L12,21 L17,26 L17,10 L12,15 L8,15 L8,21 Z");
            self.volumeSVG.querySelector(".ytp-svg-sound-mute-group").style.opacity = 1;

        }

        //ytp-svg-13
        //tamaçık
        //M8,21 L12,21 L17,26 L17,10 L12,15 L8,15 L8,21 Z M19,14 L19,22 C20.48,21.32 21.5,19.77 21.5,18 C21.5,16.26 20.48,14.74 19,14 Z M19,11.29 C21.89,12.15 24,14.83 24,18 C24,21.17 21.89,23.85 19,24.71 L19,26.77 C23.01,25.86 26,22.28 26,18 C26,13.72 23.01,10.14 19,9.23 L19,11.29 Z

        //orta
        //M8,21 L12,21 L17,26 L17,10 L12,15 L8,15 L8,21 Z M19,14 L19,22 C20.48,21.32 21.5,19.77 21.5,18 C21.5,16.26 20.48,14.74 19,14 Z

        //kapalı


    }

    setTimeout( resizetimer , 1000 );

    function resizetimer(){

        resizeuygula();

        setTimeout( resizetimer , 400 );

    }

    function resizeuygula(){

        var container_width = self.videoelement.offsetWidth;
        var yeni_container_height = container_width / ( self.aktifaspect);

        self.videocontainer.style.height = yeni_container_height + "px";
        self.videoelement.style.height = "100%";

    }

    window.onresize = function(event){

        console.log("window on resize");

        resizeuygula();

    };

    this.yayinmodu = "live";


    console.log(this.ayarlar);

    self.hls = undefined;

    if (this.ayarlar.vod !== undefined){
        this.yayinmodu = "vod";
    } else if (this.ayarlar.m3u8_on_demand !== undefined){
        this.yayinmodu = "m3u8_on_demand";
    } else {
        this.sliderContainer.style.display = "none";
        this.ilgilivideolarContainer.style.display = "none";
        this.ilgilivideolarButton.style.display = "none";


    }

    if (self.yayinmodu !== "live") {

        this.videothumbcontainer = this.elementOlustur(this.playerOverlay, "div", ["video-thumb-container"] );
        this.thumbalan = this.elementOlustur(this.videothumbcontainer, "div", ["video-thumb"]);

    }


    self.aktifaspect = 16/9;

    self.duration = 0;
    self.videoelement.addEventListener( "loadedmetadata", function (e) {
        var width = this.videoWidth,
            height = this.videoHeight;
        self.aktifaspect = width / height;

        console.log(width + "x" + height);

        var container_width = self.videoelement.offsetWidth;
        var yeni_container_height = container_width / ( self.aktifaspect);
        console.log("yeni height: " + yeni_container_height);
        self.videocontainer.style.height = yeni_container_height + "px";
        self.videoelement.style.height = "100%";

    }, false );

    var forceRedraw = function(element){
        var disp = element.style.display;
        element.style.display = 'none';
        var trick = element.offsetHeight;
        element.style.display = disp;
    };
    self.videoelement.onplay = function() {
        console.log("The video has started to play");

        //M 12,26 16,26 16,10 12,10 z M 21,26 25,26 25,10 21,10 z (pause)

        self.playDugmesi.querySelector(".ytp-svg-fill").setAttribute("d","M 12,26 16,26 16,10 12,10 z M 21,26 25,26 25,10 21,10 z");

        forceRedraw(self.playDugmesi);

    };
    self.videoelement.onpause = function() {
        console.log("The video has been paused");
        //M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z (play)
        self.playDugmesi.querySelector(".ytp-svg-fill").setAttribute("d","M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z");
        forceRedraw(self.playDugmesi);
    };
    self.videoelement.onstalled = function() {
        //console.log("Media data is not available");
        //M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z (play)
        //self.playDugmesi.querySelector(".ytp-svg-fill").setAttribute("d","M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z");
        //forceRedraw(self.playDugmesi);
    };
    self.videoelement.onended = function() {

        if ( (self.ayarlar.vod !== undefined && self.ayarlar.vod.related !== undefined) || (self.ayarlar.m3u8_on_demand !== undefined && self.ayarlar.m3u8_on_demand.related !== undefined) ){

            self.ilgilivideolarOverlay.style.display = "block";

        }


        console.log("The audio has ended");
        //M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z (play)
        self.playDugmesi.querySelector(".ytp-svg-fill").setAttribute("d","M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z");
        forceRedraw(self.playDugmesi);
    };
    self.videoelement.onerror = function() {
        console.log("Error! Something went wrong");
        //self.playDugmesi.querySelector(".ytp-svg-fill").setAttribute("d","M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z");
        forceRedraw(self.playDugmesi);
    };
    self.videoelement.waiting = function() {
        console.log("Wait! I need to buffer the next frame");
    };
    self.videoelement.ontimeupdate = function() {
        //console.log("currentTime: " +  self.videoelement.currentTime );
        self.konumuguncelle();
    };
    self.videoelement.ondurationchange = function() {
        //console.log("duration: " + self.videoelement.duration );
        self.duration = self.ayarlar.sure === undefined ? self.videoelement.duration : self.ayarlar.sure / 1000;

        if (self.ayarlar.vod !== undefined){
            self.sliderContainer.style.display = "block";
        } else if (self.ayarlar.m3u8_on_demand !== undefined){
            self.sliderContainer.style.display = "block";
        } else {
            self.sliderContainer.style.display = "none";
        }


        self.konumuguncelle();
    };
    self.videoelement.onvolumechange = function() {
        self.sesAyari.value  = self.videoelement.volume * 100;

        sesAyari();
    };
    self.sesAyari.value  = self.videoelement.volume * 100;


    self.seconds2time = function  (seconds) {
        var hours   = Math.floor(seconds / 3600);
        var minutes = Math.floor((seconds - (hours * 3600)) / 60);
        var seconds = seconds - (hours * 3600) - (minutes * 60);
        var time = "";

        if (isNaN(hours)) return "";
        if (isNaN(minutes)) return "";
        if (isNaN(seconds)) return "";

        if (hours != 0) {
            time = hours+":";
        }
        if (minutes != 0 || time !== "") {
            minutes = (minutes < 10 && time !== "") ? "0"+minutes : String(minutes);
            time += minutes+":";
        }
        if (time === "") {
            time = seconds+"s";
        }
        else {
            time += (seconds < 10) ? "0"+seconds : String(seconds);
        }
        return time;
    }

    self.konumuguncelle = function(){

        if (self.videoelement.duration === undefined || self.videoelement.currentTime === undefined ){

        }else{

            var oran = (self.videoelement.currentTime / self.videoelement.duration) * 10000;

            if (isNaN(parseInt(self.videoelement.currentTime))) return;
            if (isNaN(parseInt(self.videoelement.duration))) return;

            var currentTimeBilgi = self.seconds2time(parseInt(self.videoelement.currentTime));
            var    durationBilgi = self.seconds2time(parseInt(self.videoelement.duration));

            //console.log(currentTimeBilgi + " - " + durationBilgi);


            this.yayinMetinAlani.style.display = "inline-block";

            if (this.yayinmodu == "vod" || this.yayinmodu === "m3u8_on_demand"){
                this.yayinMetinAlani.innerHTML = currentTimeBilgi + " - " + durationBilgi;
                console.log(self.videoelement.currentTime , self.videoelement.duration );

            }else{
                this.yayinMetinAlani.innerHTML = "CANLI";

            }

            if (self.slideradokunma == false){
                self.slider.value = oran;
            }

        }

    }

    self.anabasliklarlistesi = [];
    self.alternatif_yayinlar = [];
    self.alternatif_yayinlar_hls = [];
    self.dillerlistesi = [];

    var ana_menu;
    var alternatif_yayinlar_menu;
    var alternatif_yayinlar_hls_menu;
    var diller_listesi_menu;

    self.oncekimenu = undefined;
    self.aktifmenu = undefined;

    self.menudegistir_detayli = function(menudetay,sifirla) {

        self.oncekimenu = self.aktifmenu;
        self.menudegistir (menudetay);

    }

    self.kalitemenusuGosterGizle = function() {
        if (self.kaliteAyarlari.style.display == 'none'){
            self.kalitemenusuGoster();
        }else{
            self.kalitemenusuGizle();
        }
    }
    self.kalitemenusuGizle = function() {

        self.kaliteAyarlari.style.display = 'none';

    }
    self.kalitemenusuGoster = function() {

        self.kaliteAyarlari.style.display = 'block';

    }

    self.kalitedegistir = function(i) {

        self.secilikalitelabel = i;

        if (self.yayinmodu == "vod"){
            console.log(i);
            console.log(self.alternatif_yayinlar[i]);

            console.log(self.videoelement.paused);
            console.log(self.videoelement.currentTime);

            var oncekivakit = self.videoelement.currentTime;

            if (self.videoelement.paused){

                self.videosourceAta( self.alternatif_yayinlar[i] , false );
                self.videoelement.load();

            }else{

                console.log(self.videoelement.currentTime);
                self.videosourceAta( self.alternatif_yayinlar[i] , true );
                self.videoelement.load();

            }
            self.videoelement.currentTime = oncekivakit;

//					if (this.videoelement.paused)
            //vod_self.videosourceAta( baslatilacak_source ,autoplay);


        }
        if (self.yayinmodu === "live" || self.yayinmodu === "m3u8_on_demand"){
            self.hls.nextLevel = i-1;
            self.ayarmenusunukur();
        }

        self.kalitemenusuGizle();

    };

    self.playDugmesi.onclick = function(e){

        if (self.yayinmodu === "vod"){
            if (self.videoelement.paused){

                setTimeout(function () {
                    if (self.videoelement.paused) {
                        self.videoelement.play();
                    }
                }, 150);

            }else{

                setTimeout(function () {
                    if (!self.videoelement.paused) {
                        self.videoelement.pause();
                    }
                }, 150);

            }
        }else{

            playpause();

        }
        e.stopPropagation();

    }

    self.secilikalitelabel = 0;

    function kalitetik(e){
        self.kalitedegistir( this.getAttribute("refid") );

        if (e){ e.stopPropagation();  } else{ window.event.cancelBubble = true; }
    }
    self.kalitemenusu = function(menudetay) {
        //self.ayarlaroncekiAyar = self.elementOlustur(self.playerOverlay, "div", ["canli-yayin-bilgi-alani"] );

        console.log(menudetay);


        var metin = "<ul style='color: white;'>";

        for (var i = 0; i < menudetay.length; i++) {
            var obj = menudetay[i];
            var baslik = obj.label || obj.name || "Menu";

            var ekclass = "";
            if (i == self.secilikalitelabel){
                ekclass = " secili_kalite ";
            }
            metin += "<li class='kaliteclass " + ekclass + "' refid='" + i + "' data-target='icmenu" + i + "'>";
            metin += "<div>";
            metin += baslik;
            metin += "</div>";
            metin += "</li>";
        }
        metin += "</ul>";
        self.kaliteAyarlari.innerHTML = metin;

        var itemler = document.querySelectorAll(".kaliteclass");
        for (i = 0; i < itemler.length; i++) {
            itemler[i].onclick = kalitetik ;
        }

        aktifmenu = menudetay;

    }

    self.ayarmenusunukur = function(){
        //1- settings menüsü => canlı yayın için kalite, dil, vod için kalite

        if (this.yayinmodu === "vod"){
            if (self.alternatif_yayinlar.length > 0){
                self.kalitemenusu (self.alternatif_yayinlar);
            }
        }
        if (this.yayinmodu === "live" || this.yayinmodu === "m3u8_on_demand"){
            if (self.alternatif_yayinlar.length > 0){
                self.kalitemenusu (self.alternatif_yayinlar);
            }
        }


    }


    //live hls yükle
    function loadScript(url, callback){

        var script = document.createElement("script");
        script.type = "text/javascript";

        if (script.readyState){  //IE
            script.onreadystatechange = function(){
                if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function(){
                callback();
            };
        }

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    }

    if (this.yayinmodu == "vod"){


        console.log(this.ayarlar.vod);
        console.log(this.ayarlar.vod.playlist);
        if (this.ayarlar.vod.playlist === undefined){
            return;
        }


        var vod_self = this;
        vod_self.videosourceAta = function (videosource, autoplay){


            var source = document.createElement('source');
            source.setAttribute('src', videosource.file );
            this.videoelement.innerHTML = "";
            this.videoelement.appendChild(source);

            console.log(self.ayarlar);

            if (self.ayarlar.preload){

                this.videoelement.setAttribute('preload', self.ayarlar.preload );

            }

            if(autoplay !== undefined && autoplay == true){
                this.videoelement.setAttribute('autoplay', "true" );
            }else{
                this.videoelement.removeAttribute('autoplay');
            }

            self.ayarmenusunukur();

        }

        vod_self.icerigigetir = function (playlistnesne){

            self.playlistnesne = playlistnesne;

            var ilknesne;

            var baslatilacak_source;

            var autoplay = false;

            if (typeof playlistnesne == "object"){
                ilknesne = playlistnesne[0];
            }

            if (ilknesne.sources.length > 1){
                self.kaliteDugmesi.style.display = "inline-block";
            }else{
                self.kaliteDugmesi.style.display = "none";
            }

            if (ilknesne.autoplay !== undefined){
                if ( ilknesne.autoplay == true || ilknesne.autoplay == "true" ){
                    autoplay = true;
                }
            }

            console.log(ilknesne);

            if (ilknesne.image !== undefined){
                self.videoelement.setAttribute("poster",ilknesne.image);
                console.log(ilknesne.image);
            }

            if (ilknesne.sources !== undefined){

                self.alternatif_yayinlar = [];

                if (Object.prototype.toString.call( ilknesne.sources )  === '[object Array]'){
                    self.alternatif_yayinlar = ilknesne.sources;
                }else if (typeof ilknesne.sources === "string"){
                    self.alternatif_yayinlar.push({
                        "file": ilknesne.sources
                    });
                }else{
                    self.alternatif_yayinlar.push( ilknesne.sources );
                }

                for (var i = 0; i < ilknesne.sources.length; i++) {
                    var obj = ilknesne.sources[i];
                    if ( i == 0 || (obj.file !== undefined && (obj.default == true || obj.default == "true"))){
                        baslatilacak_source = obj;
                        self.secilikalitelabel = i;

                    }
                }

            }

            vod_self.videosourceAta( baslatilacak_source ,autoplay);




        }


        if (typeof this.ayarlar.vod.playlist === "string"){

            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function()
            {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        //success(JSON.parse(xhr.responseText));
                        vod_self.icerigigetir( JSON.parse(xhr.responseText) );
                    } else {
                        console.log("playlist yükleme hatası: ");
                        console.log( xhr );
                        //error(xhr);
                    }
                }
            };
            xhr.open("GET", this.ayarlar.vod.playlist , true);
            xhr.send();

        }else if (typeof this.ayarlar.vod.playlist === "object"){

            vod_self.icerigigetir( this.ayarlar.vod.playlist );

        }



        if (this.ayarlar.vod.related !== undefined){

            vod_self.relatedGetir = function( icerik ){

                console.log("relatedGetir icerik");
                console.log(icerik);



                var tdler = [];


                for (var i = 0; i < icerik.length; i++){

                    if (i>11) continue;

                    var aktiftd = "";

                    aktiftd += "<td style='width: 25% !important; position: relative; vertical-align: center; box-sizing: border-box; padding: 0px;'>";

                    aktiftd += "<div style=' box-sizing: border-box;z-index: 900; position: absolute;left:0px;top:0px;right:0px;bottom:0px;background-image:url(" + icerik [i].image + ");background-size: cover; background-repeat: no-repeat; background-position: center center;'>";
                    aktiftd += "</div>";
                    aktiftd += "<div class='bggrad' style='box-sizing: border-box; width: 100%; height: 100%; z-index: 999; position: relative; text-align: left;'>";

                    aktiftd += "<a href='" + icerik[i].link + "' style='text-decoration: none; color: white;'>";

                    aktiftd += "<div style='box-sizing: border-box;padding: 10px;width: 100%; height: 100%; text-shadow: 1px 1px 1px rgba(0, 0, 0, 1); text-overflow: ellipsis;'>";
                    aktiftd += icerik[i].title;
                    aktiftd += "</div>";

                    aktiftd += "</a>";

                    aktiftd += "</div>";

                    aktiftd += "</td>";

                    tdler.push(aktiftd);

                    console.log(icerik [i]);
                    /*
                     innerHTML += "<div style='width: 140px; height: 100px; position:relative; margin: 10px;'>";
                     innerHTML += "<div class='bggrad' style='position: absolute; box-sizing: border-box; text-align: left; text-overflow: ellipsis; padding: 15px; z-index: 9999; color: white; font-size: 0.9vw; left: 0px; top: 0px; right: 0px; bottom: 0px;  width: 100%; height: 100%;'>";

                     var baslik = self.wordwrap(icerik[i].title, 70, '\n').split('\n')[0].trim();


                     innerHTML += baslik + "...";
                     innerHTML += "</div>";
                     innerHTML += "</div>";
                     */

                    //self.ilgilivideolarContainer

                }

                if (tdler.length < 8){
                    for (var i = 0; i < 8 - tdler.length; i++) {
                        var aktiftd = "";
                        aktiftd += "<td>";
                        aktiftd += "</td>";
                        tdler.push(aktiftd);
                    }
                }

                var innerHTML  = "<table style=' margin-top: 20px; height: calc(100% - 44px); width: 100%;'>";
                innerHTML  += " <tr style='height: 33%;'>";
                innerHTML  += tdler[0];
                innerHTML  += tdler[1];
                innerHTML  += tdler[2];
                innerHTML  += tdler[3];
                innerHTML  += " </tr>";
                innerHTML  += " <tr style='height: 33%;'>";
                innerHTML  += tdler[4];
                innerHTML  += tdler[5];
                innerHTML  += tdler[6];
                innerHTML  += tdler[7];
                innerHTML  += " </tr>";
                innerHTML  += " <tr style='height: 33%;'>";
                innerHTML  += tdler[8];
                innerHTML  += tdler[9];
                innerHTML  += tdler[10];
                innerHTML  += tdler[11];
                innerHTML  += " </tr>";
                innerHTML  += "</table>";

                self.ilgilivideolarContainer.innerHTML = innerHTML;

            }

            if (typeof this.ayarlar.vod.related === "string"){

                var xhr1 = new XMLHttpRequest();
                xhr1.onreadystatechange = function()
                {
                    if (xhr1.readyState === XMLHttpRequest.DONE) {
                        if (xhr1.status === 200) {
                            //success(JSON.parse(xhr.responseText));
                            vod_self.relatedGetir( JSON.parse(xhr1.responseText) );
                        } else {
                            console.log("related yükleme hatası: ");
                            console.log( xhr1 );
                            //error(xhr);
                        }
                    }
                };
                xhr1.open("GET", this.ayarlar.vod.related , true);
                xhr1.send();

            }

        }else{
            this.ilgilivideolarButton.style.display = "none";
        }

    }else if (self.yayinmodu=== "m3u8_on_demand") {

        var poster     = this.ayarlar.m3u8_on_demand.image;
        var url        = this.ayarlar.m3u8_on_demand.url;
        var autoplay   = this.ayarlar.m3u8_on_demand.autoplay;
        var preload    = this.ayarlar.m3u8_on_demand.preload;


        self.initm3u8hls = function (){


            if(Hls.isSupported()) {
                if (self.hls) {
                    self.hls.destroy();
                    if (self.hls.bufferTimer) {
                        clearInterval(hls.bufferTimer);
                        self.hls.bufferTimer = undefined;
                    }
                    self.hls = null;
                }



                if (poster !== undefined){
                    self.videoelement.setAttribute( "poster" , poster );
                    console.log(poster);
                }


                self.hls = new Hls({
                    debug:false,
                    enableWorker : true,
                    defaultAudioCodec : undefined ,
                    nudgeMaxRetry: 999,
                    fragLoadingMaxRetry: 999,
                    pLoader: function (config) {
                        let loader = new Hls.DefaultConfig.loader(config);

                        this.abort = () => loader.abort();
                        this.destroy = () => loader.destroy();
                        this.load = (context, config, callbacks) => {
                            let {type, url} = context;

                            if (url.indexOf("?") > -1){
                                url = url + "&sessionid=" + self.player_unique_id;
                            }else{
                                url = url + "?sessionid=" + self.player_unique_id;
                            }

                            if (type === 'manifest') {
                                console.log(`Manifest ${url} will be loaded.`);
                            }

                            context.url = url;

                            loader.load(context, config, callbacks);
                        };
                    },
                    fLoader: function (config) {
                        let loader = new Hls.DefaultConfig.loader(config);

                        this.abort = () => loader.abort();
                        this.destroy = () => loader.destroy();
                        this.load = (context, config, callbacks) => {
                            let {type, url} = context;

                            if (url.indexOf("?") > -1){
                                url = url + "&sessionid=" + self.player_unique_id;
                            }else{
                                url = url + "?sessionid=" + self.player_unique_id;
                            }

                            if (type === 'manifest') {
                                console.log(`Manifest ${url} will be loaded.`);
                            }

                            context.url = url;

                            loader.load(context, config, callbacks);
                        };
                    },
                });
                self.hls.mounted = false;

                var video = self.videoelement;

                self.hls.hedefurl = url;
                self.hls.hedefelement = video;

                //self.hls.loadSource(url);
                //self.hls.attachMedia(video);


                self.hls.on(Hls.Events.MEDIA_ATTACHED,function() {
                    console.log('MediaSource attached...');
                });
                self.hls.on(Hls.Events.MEDIA_DETACHED,function() {
                    console.log('MediaSource detached...');
                });
                self.hls.on(Hls.Events.LEVEL_SWITCH,function(event,data) {
                    console.log('LEVEL_SWITCH...');
                    //events.level.push({time : performance.now() - events.t0, id : data.level, bitrate : Math.round(hls.levels[data.level].bitrate/1000)});
                    //updateLevelInfo();
                });
                self.hls.on(Hls.Events.MANIFEST_PARSED,function(event,data) {
                    console.log('MANIFEST_PARSED...');
                    self.alternatif_yayinlar.push({
                        name: "Auto"
                    });
                    for (var i = 0; i < self.hls.levels.length; i++) {
                        var obj = self.hls.levels[i];
                        self.alternatif_yayinlar.push(obj);

                    }
                    self.hls.autoLevelCapping = -1;
                    self.hls.nextLevel = -1;

                    self.ayarmenusunukur();
                });
                self.hls.on(Hls.Events.LEVEL_LOADED,function(event,data) {
                    //console.log('LEVEL_LOADED...');
                });
                self.hls.on(Hls.Events.FRAG_BUFFERED,function(event,data) {
                    //console.log('FRAG_BUFFERED...');
                });
                self.hls.on(Hls.Events.FRAG_CHANGED,function(event,data) {
                    console.log('FRAG_CHANGED...');
                });
                self.hls.on(Hls.Events.FRAG_LOAD_EMERGENCY_ABORTED,function(event,data) {
                    //console.log('FRAG_LOAD_EMERGENCY_ABORTED...');
                });
                self.hls.on(Hls.Events.FRAG_DECRYPTED,function(event,data) {
                    //console.log('FRAG_LOAD_EMERGENCY_ABORTED...');
                });
                self.hls.on(Hls.Events.ERROR, function(event,data) {
                    console.log("ERROR");
                    console.log(data);
                    console.log(event);
                });
                self.hls.on(Hls.Events.BUFFER_CREATED, function(event,data) {
                    console.log("BUFFER_CREATED");
                });

                //if (autoplay === true){
                console.log("AUTO PLAY");
                self.playlive();
                //}


            }

        }

        if (typeof Hls == 'undefined'){
            loadScript('/assets/playerjs/hls.min.js', function(){
                //Stuff to do after someScript has loaded
                console.log("hls yüklendi");

                self.initm3u8hls();

            });

        } else {
            console.log("hls zaten yüklü");
            self.initm3u8hls();
        }

        if (this.ayarlar.m3u8_on_demand.related !== undefined){

            self.relatedGetir = function( icerik ){

                console.log("self relatedGetir icerik");


                var tdler = [];


                for (var i = 0; i < icerik.length; i++){

                    if (i>11) continue;

                    var aktiftd = "";

                    aktiftd += "<td style='width: 25% !important; position: relative; vertical-align: center; box-sizing: border-box; padding: 0px;'>";

                    aktiftd += "<div style=' box-sizing: border-box;z-index: 900; position: absolute;left:0px;top:0px;right:0px;bottom:0px;background-image:url(" + icerik [i].image + ");background-size: cover; background-repeat: no-repeat; background-position: center center;'>";
                    aktiftd += "</div>";
                    aktiftd += "<div class='bggrad' style='box-sizing: border-box; width: 100%; height: 100%; z-index: 999; position: relative; text-align: left;'>";

                    aktiftd += "<a href='" + icerik[i].link + "' style='text-decoration: none; color: white;'>";

                    aktiftd += "<div style='box-sizing: border-box;padding: 10px;width: 100%; height: 100%; text-shadow: 1px 1px 1px rgba(0, 0, 0, 1); text-overflow: ellipsis;'>";
                    aktiftd += icerik[i].title;
                    aktiftd += "</div>";

                    aktiftd += "</a>";

                    aktiftd += "</div>";

                    aktiftd += "</td>";

                    tdler.push(aktiftd);

                    //console.log(icerik [i]);
                    /*
                     innerHTML += "<div style='width: 140px; height: 100px; position:relative; margin: 10px;'>";
                     innerHTML += "<div class='bggrad' style='position: absolute; box-sizing: border-box; text-align: left; text-overflow: ellipsis; padding: 15px; z-index: 9999; color: white; font-size: 0.9vw; left: 0px; top: 0px; right: 0px; bottom: 0px;  width: 100%; height: 100%;'>";

                     var baslik = self.wordwrap(icerik[i].title, 70, '\n').split('\n')[0].trim();


                     innerHTML += baslik + "...";
                     innerHTML += "</div>";
                     innerHTML += "</div>";
                     */

                    //self.ilgilivideolarContainer

                }

                if (tdler.length < 8){
                    for (var i = 0; i < 8 - tdler.length; i++) {
                        var aktiftd = "";
                        aktiftd += "<td>";
                        aktiftd += "</td>";
                        tdler.push(aktiftd);
                    }
                }

                var innerHTML  = "<table style=' margin-top: 20px; height: calc(100% - 44px); width: 100%;'>";
                innerHTML  += " <tr style='height: 33%;'>";
                innerHTML  += tdler[0];
                innerHTML  += tdler[1];
                innerHTML  += tdler[2];
                innerHTML  += tdler[3];
                innerHTML  += " </tr>";
                innerHTML  += " <tr style='height: 33%;'>";
                innerHTML  += tdler[4];
                innerHTML  += tdler[5];
                innerHTML  += tdler[6];
                innerHTML  += tdler[7];
                innerHTML  += " </tr>";
                innerHTML  += " <tr style='height: 33%;'>";
                innerHTML  += tdler[8];
                innerHTML  += tdler[9];
                innerHTML  += tdler[10];
                innerHTML  += tdler[11];
                innerHTML  += " </tr>";
                innerHTML  += "</table>";

                self.ilgilivideolarContainer.innerHTML = innerHTML;

            }

            if (typeof this.ayarlar.m3u8_on_demand.related === "string"){

                var xhr1 = new XMLHttpRequest();
                xhr1.onreadystatechange = function()
                {
                    if (xhr1.readyState === XMLHttpRequest.DONE) {
                        if (xhr1.status === 200) {
                            //success(JSON.parse(xhr.responseText));
                            self.relatedGetir( JSON.parse(xhr1.responseText) );
                        } else {
                            console.log("related yükleme hatası: ");
                            console.log( xhr1 );
                            //error(xhr);
                        }
                    }
                };
                xhr1.open("GET", this.ayarlar.m3u8_on_demand.related , true);
                xhr1.send();

            }

        }else{
            this.ilgilivideolarButton.style.display = "none";
        }


    }else if (self.yayinmodu=== "live") {



        self.inithls = function(){



            if(Hls.isSupported()) {
                if (self.hls) {
                    self.hls.destroy();
                    if (self.hls.bufferTimer) {
                        clearInterval(hls.bufferTimer);
                        self.hls.bufferTimer = undefined;
                    }
                    self.hls = null;
                }


                var ilknesne;

                var baslatilacak_source;

                var autoplay = false;

                if (typeof self.ayarlar.live.playlist == "object"){
                    ilknesne = self.ayarlar.live.playlist[0];
                }


                if (ilknesne.autoplay !== undefined){
                    if ( ilknesne.autoplay == true || ilknesne.autoplay == "true" ){
                        autoplay = true;
                    }
                }

                console.log(ilknesne);

                if (ilknesne.image !== undefined){
                    self.videoelement.setAttribute( "poster" , ilknesne.image );
                    console.log(ilknesne.image);
                }

                var url = "";

                if (ilknesne.url !== undefined){

                    url = ilknesne.url;

                }


                self.hls = new Hls({
                    debug:false,
                    enableWorker : true,
                    defaultAudioCodec : undefined ,
                    nudgeMaxRetry: 999,
                    liveSyncDurationCount: 5,
                    initialLiveManifestSize: 2,
                    fragLoadingMaxRetry: 999,
                    startFragPrefetch: true,

                    pLoader: function (config) {
                        let loader = new Hls.DefaultConfig.loader(config);

                        this.abort = () => loader.abort();
                        this.destroy = () => loader.destroy();
                        this.load = (context, config, callbacks) => {
                            let {type, url} = context;

                            if (url.indexOf("?") > -1){
                                url = url + "&sessionid=" + self.player_unique_id;
                            }else{
                                url = url + "?sessionid=" + self.player_unique_id;
                            }

                            if (type === 'manifest') {
                                console.log(`Manifest ${url} will be loaded.`);
                            }

                            context.url = url;

                            loader.load(context, config, callbacks);
                        };
                    },
                    fLoader: function (config) {
                        let loader = new Hls.DefaultConfig.loader(config);

                        this.abort = () => loader.abort();
                        this.destroy = () => loader.destroy();
                        this.load = (context, config, callbacks) => {
                            let {type, url} = context;

                            if (url.indexOf("?") > -1){
                                url = url + "&sessionid=" + self.player_unique_id;
                            }else{
                                url = url + "?sessionid=" + self.player_unique_id;
                            }

                            if (type === 'manifest') {
                                console.log(`Manifest ${url} will be loaded.`);
                            }

                            context.url = url;

                            loader.load(context, config, callbacks);
                        };
                    },

                });
                self.hls.mounted = false;

                var video = self.videoelement;

                self.hls.hedefurl = url;
                self.hls.hedefelement = video;

                //self.hls.loadSource(url);
                //self.hls.attachMedia(video);



                self.hls.on(Hls.Events.MEDIA_ATTACHED,function() {
                    console.log('MediaSource attached...');
                });
                self.hls.on(Hls.Events.MEDIA_DETACHED,function() {
                    console.log('MediaSource detached...');
                });
                self.hls.on(Hls.Events.LEVEL_SWITCH,function(event,data) {
                    console.log('LEVEL_SWITCH...');
                    //events.level.push({time : performance.now() - events.t0, id : data.level, bitrate : Math.round(hls.levels[data.level].bitrate/1000)});
                    //updateLevelInfo();
                });
                self.hls.on(Hls.Events.MANIFEST_PARSED,function(event,data) {
                    console.log('MANIFEST_PARSED...');
                    self.alternatif_yayinlar.push({
                        name: "Auto"
                    });
                    for (var i = 0; i < self.hls.levels.length; i++) {
                        var obj = self.hls.levels[i];
                        self.alternatif_yayinlar.push(obj);

                    }
                    self.hls.autoLevelCapping = -1;
                    self.hls.nextLevel = -1;

                    self.ayarmenusunukur();
                });
                self.hls.on(Hls.Events.LEVEL_LOADED,function(event,data) {
                    //console.log('LEVEL_LOADED...');
                });
                self.hls.on(Hls.Events.FRAG_BUFFERED,function(event,data) {
                    //console.log('FRAG_BUFFERED...');
                });
                self.hls.on(Hls.Events.FRAG_CHANGED,function(event,data) {
                    console.log('FRAG_CHANGED...');
                });
                self.hls.on(Hls.Events.FRAG_LOAD_EMERGENCY_ABORTED,function(event,data) {
                    //console.log('FRAG_LOAD_EMERGENCY_ABORTED...');
                });
                self.hls.on(Hls.Events.FRAG_DECRYPTED,function(event,data) {
                    //console.log('FRAG_LOAD_EMERGENCY_ABORTED...');
                });
                self.hls.on(Hls.Events.ERROR, function(event,data) {
                    console.log("ERROR");
                    console.log(data);
                    console.log(event);
                });
                self.hls.on(Hls.Events.BUFFER_CREATED, function(event,data) {
                    console.log("BUFFER_CREATED");
                });

                if (this.ayarlar !== undefined && this.ayarlar.autoplay !== undefined && this.ayarlar.autoplay === true){
                    console.log("AUTO PLAY");
                    self.playlive();
                }


            }};

        if (typeof Hls == 'undefined'){
            loadScript('/assets/playerjs/hls.min.js', function(){
                //Stuff to do after someScript has loaded
                console.log("hls yüklendi");

                self.inithls();

            });

        } else {
            console.log("hls zaten yüklü");
            self.inithls();
        }



    }


    self.wordwrap = function ( str, width, brk, cut ) {

        brk = brk || 'n';
        width = width || 75;
        cut = cut || false;

        if (!str) { return str; }

        var regex = '.{1,' +width+ '}(\s|$)' + (cut ? '|.{' +width+ '}|.+$' : '|\S+?(\s|$)');

        return str.match( RegExp(regex, 'g') ).join( brk );

    }

    return self;

};

var FX = {
    easing: {
        linear: function(progress) {
            return progress;
        },
        quadratic: function(progress) {
            return Math.pow(progress, 2);
        },
        swing: function(progress) {
            return 0.5 - Math.cos(progress * Math.PI) / 2;
        },
        circ: function(progress) {
            return 1 - Math.sin(Math.acos(progress));
        },
        back: function(progress, x) {
            return Math.pow(progress, 2) * ((x + 1) * progress - x);
        },
        bounce: function(progress) {
            for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
                if (progress >= (7 - 4 * a) / 11) {
                    return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
                }
            }
        },
        elastic: function(progress, x) {
            return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * x / 3 * progress);
        }
    },
    animate: function(options) {
        var start = new Date;
        var id = setInterval(function() {
            var timePassed = new Date - start;
            var progress = timePassed / options.duration;
            if (progress > 1) {
                progress = 1;
            }
            options.progress = progress;
            var delta = options.delta(progress);
            options.step(delta);
            if (progress == 1) {
                clearInterval(id);
                options.complete();
            }
        }, options.delay || 10);
    },
    fadeOut: function(element, options) {
        var to = 1;
        this.animate({
            duration: options.duration,
            delta: function(progress) {
                progress = this.progress;
                return FX.easing.swing(progress);
            },
            complete: options.complete,
            step: function(delta) {
                element.style.opacity = to - delta;
            }
        });
    },
    fadeIn: function(element, options) {
        var to = 0;
        this.animate({
            duration: options.duration,
            delta: function(progress) {
                progress = this.progress;
                return FX.easing.swing(progress);
            },
            complete: options.complete,
            step: function(delta) {
                element.style.opacity = to + delta;
            }
        });
    }
};
window.FX = FX;

