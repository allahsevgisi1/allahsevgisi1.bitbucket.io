<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:fb="http://ogp.me/ns/fb#"
      xmlns="http://www.w3.org/1999/xhtml"  >

<!-- Mirrored from allahsevgisi.net/tr/Ahir-Zamana-ait-Yeni-Bilgiler/21432/Sayın_Adnan_Oktar%27ın,_konuşmalarında_İslam_ahlakının_hakimiyeti_ve_Mehdiyet_konularına_özel_yer_ayırması,_Kuran_ahlakının_bir_gereği_ve_Peygamberimiz_(sav)%27in_bir_sünnetidir by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Jul 2021 21:58:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sayın Adnan Oktar'ın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)'in bir sünnetidir</title>
<meta name="keywords" content="" />
<meta name="description" content="Sayın Adnan Oktarın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)in bir sünnetidir video izle, Sayın Adnan Oktarın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)in bir sünnetidir hakkında adnan oktar yorumları, düşünceleri nedir Sayın Adnan Oktarın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)in bir sünnetidir ile ilgili yazılar, makaleler, röportajlar ve  harun yahya belgeselleri seyret, facebookta paylaş, twitterda paylaş." />
<meta property="og:type" content="article">
<meta name="apple-itunes-app" content="app-id=432879700"/>
<meta property="og:title" content="Sayın Adnan Oktarın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)in bir sünnetidir - Allahsevgisi.net"/>   
<meta property="og:url" content="sayin-adnan-oktarin-konusmalarinda-islam.html" />
<meta property="og:image" content="http://g.fmanager.net/Image/objects/43-ahir-zamana-ait-yeni-bilgiler/21432_sayin_adnan_oktar_in_konusmalarinda_islam_ahlakinin_hakimiyeti_ve_mehdiyet_konularina_ozel_yer.jpg"/>
<!--<meta property="og:image:type" content="image/jpeg" />-->
<!--<meta property="og:image:width" content="300" />-->
<!--<meta property="og:image:height" content="300" />-->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@adnan_oktar">
    <meta name="twitter:creator" content="@adnan_oktar">
    <meta name="twitter:title" content="Sayın Adnan Oktarın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)in bir sünnetidir - Allahsevgisi.net">
    <meta name="twitter:description" content="Sayın Adnan Oktarın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)in bir sünnetidir video izle, Sayın Adnan Oktarın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)in bir sünnetidir hakkında adnan oktar yorumları, düşünceleri nedir Sayın Adnan Oktarın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)in bir sünnetidir ile ilgili yazılar, makaleler, röportajlar ve  harun yahya belgeselleri seyret, facebookta paylaş, twitterda paylaş.">
    <meta name="twitter:image" content="http://g.fmanager.net/Image/objects/43-ahir-zamana-ait-yeni-bilgiler/21432_sayin_adnan_oktar_in_konusmalarinda_islam_ahlakinin_hakimiyeti_ve_mehdiyet_konularina_ozel_yer.jpg">

<meta name="robots" content="index,follow" />
<meta name="robots" content="all" />
<meta name="robots" content="index,archive" />
<meta name="author" content="Harun Yahya
" />

<!-- GOOGLE+ PROFILE  BEGIN -->
<link rel="author" href="https://plus.google.com/110138614531049716297">
<!-- GOOGLE+ PROFILE  END -->

<script type="text/javascript" src="../../../assets/js/google.jquery.1.4.2.min.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" charset="UTF-8"></script> -->


<link href="../../../assets/css/bundle_4c57a60e47096c439557efe54f29f731d0c8.css?1532788650" media="screen" rel="stylesheet" type="text/css" /><link href="../../../Image/sayfa_yonetimi/ara/1896_Allah_sevgisi/css/1896-sevgi.css" rel="stylesheet" type="text/css" />
<!--[if gt IE 7]><link href="/assets/css/book-ie.css" rel="stylesheet" type="text/css" /><![endif]-->
<!--[if lte IE 7]><link href="/assets/css/book-ie6-7.css" rel="stylesheet" type="text/css" /><![endif]-->

<!--
<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.9.0/build/slider/assets/skins/sam/slider.css" />
<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.9.0/build/fonts/fonts-min.css" />
<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.9.0/build/container/assets/skins/sam/container.css" />
<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.9.0/build/colorpicker/assets/skins/sam/colorpicker.css" />
<script type="text/javascript" src="http://yui.yahooapis.com/2.9.0/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="http://yui.yahooapis.com/2.9.0/build/connection/connection-min.js"></script>
<script type="text/javascript" src="http://yui.yahooapis.com/2.9.0/build/dragdrop/dragdrop-min.js"></script>
<script type="text/javascript" src="http://yui.yahooapis.com/2.9.0/build/animation/animation-min.js"></script>
<script type="text/javascript" src="http://yui.yahooapis.com/2.9.0/build/container/container-min.js"></script>
<script type="text/javascript" src="http://yui.yahooapis.com/2.9.0/build/slider/slider-min.js"></script>
<script type="text/javascript" src="http://yui.yahooapis.com/2.9.0/build/element/element-min.js"></script>
<script type="text/javascript" src="http://yui.yahooapis.com/2.9.0/build/colorpicker/colorpicker-min.js"></script>
-->
<link rel="canonical" href="sayin-adnan-oktarin-konusmalarinda-islam.html"/>
<script language='javascript' type='text/javascript'>if (top.location!= self.location) top.location = self.location;</script>
</head>


<body  class=''   >
        <!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5970e729ed5ee252"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'light',
    'share' : {
      'position' : 'left',
      'numPreferredServices' : 5
    },  
    'whatsnext' : {},  
    'recommended' : {
      'title': 'Sizin için önerilen:'
    } 
  });
</script>
<!-- AddThis Smart Layers END -->
<div id="bg-wrp" >
<div id="bg" >

<!--NAV-start-->
<script type="text/javascript">
 window.search=function(){
	 var searchText=document.getElementById('searchText').value;
	 if(searchText!=''){
	 	window.location = '/s/'+searchText;
	 }
	 return false;
}
</script>
  <div id="top-nav-bg"><div id="top-nav">
    <div id="top-nav-right">
    <div id="languagesWrapper"><select id="languages" class="select-language"><option value="tr" selected="selected"  rel="allahsevgisi.net" >Turkish  / Türkçe</option></select>
<script type="text/javascript">
function strpos (haystack, needle, offset) {
  var i = (haystack+'').indexOf(needle, (offset || 0));
  return i === -1 ? false : i;
}
$(document).ready(function() {
  $('#languagesWrapper #languages').change(function(){
	  var rel = $(this + "option:selected").attr("rel");
	  var pathname = window.location.pathname;
 	  if(strpos($(this).val(),"http") === false){
			var temp = pathname.split("../../../index.html");
			if( ( temp[1].length == 2 || temp[1].length == 3 ) && temp[1] != $(this).val() ){
				temp[1] = $(this).val();
				window.location = "http://"+rel+temp.join("../../../index.html");
			}else{
				window.location = "http://"+rel+window.location.pathname;
			}
	  }else{
		window.location = rel+window.location.pathname;
	  }
  });
});
</script>
</div>
    <div id="searchWrapper"><div id="search"><div id="search-left">
    <div id="search-right" onclick="search();">
      <div id="search-bg">
      <form method="get" onkeydown="if(event.keyCode==13) { search(); return false;}" onsubmit="search(); return false;" id="search">
        <input type="text" style="border: medium none; margin-top: 2px; background: none repeat scroll 0pt 0pt rgb(255, 255, 255);" 
        		value="" name="s" id="searchText" class="keyboardInput" />
        </form>
       </div>
    </div></div></div></div>
    </div>
    <div id="navmenu">
	 <ul><li><a href='../../../index.html'>Ana Sayfa
</a></li><li><a href='../../../bilgi/bize-ulasin.html'>Bize Ulaşın
</a></li></ul>    </div>
    <div class="clear"></div>
  </div></div>
<!--NAV-end-->
  <div class="center">
    <div id="header" ><a href="../../../index.html"></a></div>
    <div id="dock_wrapper">
    <!--mbcontainer buton alanı start-->
      <div id="dock"> </div>
      <div style="clear:both;"></div>
    <!--mbcontainer buton alanı end-->
	<div id="catname"><p>
    Allah Sevgisi    </p></div>
    </div>
   

<div class="doublecolumn-wrp">
    <div id="main-top"><div id="bg01"><div id="bg02"><div id="bg03"></div></div></div></div>
    <div id="main-top2col"><div id="bg01"><div id="bg02"><div id="bg03"></div></div></div></div>
</div>    
		<style>
#playerbox.wide {
	float: none;
	width: auto;
	margin: 0 10px 15px;
}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        var images = document.getElementsByTagName('img');
        for (i=0;i<images.length;i++)
        {
            //get the filename from the src
            filename = images[i].src.substring(images[i].src.lastIndexOf('../../../index.html')+1,images[i].src.lastIndexOf('.'));
            //do any formatting here
            filename = filename.replace('_',' ');
            //set alt/title tags
            var alt = images[i].getAttribute("alt");
            var title = images[i].getAttribute("title");
            if(alt != null ){
                var alt_length = alt.length;
                if(alt_length==0){
                    images[i].setAttribute('alt', filename);
                    images[i].setAttribute('title', filename);
                }else{
                    images[i].setAttribute('title', alt);
                }
            }else{
                images[i].setAttribute('alt', filename);
                images[i].setAttribute('title', filename);
            }


            /*
            if(title !=null){
             //  var title_length = title.length;
             //if(title_length==0){
              images[i].setAttribute('alt', title);
             //}
            }else{
                images[i].setAttribute('title', filename);
            }*/





        }

    })




    function showVideo(){
        obj= document.getElementById('video_desifre');
        if(obj.style.display=="none"){
            document.getElementById('video_desifre').style.display='block';
        }
        else{
            document.getElementById('video_desifre').style.display='none';
        }
    }

    function showArticles(){
        obj= document.getElementById('desifre_article');
        if(obj.style.display=="none"){
            document.getElementById('desifre_article').style.display='block';
        }
        else{
            document.getElementById('desifre_article').style.display='none';
        }

    }


</script>

<div id='info'></div>
<div id="previewBannerPlace"></div>

<div id="doublecolumn">

    <div id="playerbox" style="padding-bottom: 15px;">
        <!--playerbox baslangic-->
        <h3>Sayın Adnan Oktar'ın, konuşmalarında İslam ahlakının hakimiyeti ve Mehdiyet konularına özel yer ayırması, Kuran ahlakının bir gereği ve Peygamberimiz (sav)'in bir sünnetidir</h3>

        <!-- Social Button BEGIN -->
        <div style="text-align:right;" class="social_button_layout">
                                    <a href="https://twitter.com/adnan_oktar" class="twitter-follow-button" data-show-count="false">Follow @adnan_oktar</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        &nbsp;&nbsp;<a target="_blank" href="https://www.facebook.com/www.HarunYahya.Tv"><img alt="Harun Yahya Facebook" src="../../../assets/images/facebook_icon.png"/></a>
                            </div>

        <div>
            <center>
                <div id="container">

                </div>

                

                




            </center>

        </div>

        <!-- Social Button END -->

        <!-- PLAYER BEGIN -->
                <link rel="image_src" href="http://g.fmanager.net/Image/objects/43-ahir-zamana-ait-yeni-bilgiler/21432_sayin_adnan_oktar_in_konusmalarinda_islam_ahlakinin_hakimiyeti_ve_mehdiyet_konularina_ozel_yer.jpg" />
        <!--
        <script type="text/javascript">
            /* Player wideView */
            $(function() {
                $("#wideView").click(function() {
                    $("#playerbox").toggleClass("wide");
                    $(this).toggleClass("wide");
                });
            });
        </script>
        <a id="wideView" style="width:30px;display:none; height:20px; float:right; margin:2px 10px;"></a><div style="float:right;display: none;">Ekranı büyüt
</div>
        <div class="clear">&nbsp;</div>
        <!-- PLAYER END -->

        <!--playerbox sutun sonu-->
    </div>


    <div id="col2">
        <div id="col2inner">
            <!--sag sutun baslangic-->
            <div class='banner' id='banners_320762987891870' ><script type="text/javascript">	
$(document).ready(function(){
   $('.select-other-language').change(function(){
	 	  if(strpos($(this).val(),"http") === false){
			window.location = "http://"+$(this).val();
		  }else{
			window.location = $(this).val();
		  }
	});   
});
</script>
<div class="col2-box1"><div class="col2-box2 boxshadow4px">
<div align="center" style=" margin:15px 0;">
	<!--<h3></h3>-->
			<select id="languages"  class="select-other-language boxshadow5px" style="font-size:12px; width: 180px; height: 24px;"><option selected="selected">Diğer diller
</option><option value="az.harunyahya.org/az/eserler/180273/">Azerbaijani / Azərbaycanca</option><option value="harun-yahya.net/en/eserler/28776/">English</option></select></div>			
</div>
</div>

</div><div class="clearpad"></div><div class='banner' id='banners_3207579878980' > 

</div><div class="clearpad"></div><div class='banner' id='banners_3207569878948' ><div class="col2-box1 boxshadow4px">
    <div class="col2-box1" style="padding: 0pt;">
          <h3>Benzer Eserler</h3>
             <ul class="col2-liste">
					<li>
                <a href="../18167/Hz._Mehdi_(a.s.html">
<!--                --><!--                -->                <img src="../../../assets/images/blank.gif">
<!--                --><!--                --><!--                -->             <div class="vv">
              <h2>Hz. Mehdi (a.s.) dünyayı kötülüklerden ve zalim felsefelerden kurtaracak, insanlar arasında sevgi ve barışı hakim kılacaktır</h2>
             <em>Devamı ...
</em>
             </div>
           </a>
        </li>
        			<li>
                <a href="../20003/Hz._Mehdi_(a.s.html">
<!--                --><!--                -->                <img src="../../../assets/images/blank.gif">
<!--                --><!--                --><!--                -->             <div class="vv">
              <h2>Hz. Mehdi (a.s.) çok güç şartlar altında İslam ahlakını hakim edecektir</h2>
             <em>Devamı ...
</em>
             </div>
           </a>
        </li>
        			<li>
                <a href="../20199/Hz._Mehdi_(a.s.html">
<!--                --><!--                -->                <img src="../../../assets/images/blank.gif">
<!--                --><!--                --><!--                -->             <div class="vv">
              <h2>Hz. Mehdi (a.s.) ve talebeleri dünyanın dört bir yanına İslam ahlakını hakim edeceklerdir</h2>
             <em>Devamı ...
</em>
             </div>
           </a>
        </li>
        			<li>
                <a href="../20200/Hz._Mehdi_(a.s.html">
<!--                --><!--                -->                <img src="../../../assets/images/blank.gif">
<!--                --><!--                --><!--                -->             <div class="vv">
              <h2>Hz. Mehdi (a.s.) zamanında İslam ahlakı, bir köy evinden çöldeki çadıra kadar tüm dünyaya hakim olacaktır</h2>
             <em>Devamı ...
</em>
             </div>
           </a>
        </li>
        			<li>
                <a href="../20201/Hz._Mehdi_(a.s.)_din_ahlak%c4%b1n%c4%b1_hakim_etti%c4%9finde_Peygamberimiz_(s.a.v.html">
<!--                --><!--                -->                <img src="../../../assets/images/blank.gif">
<!--                --><!--                --><!--                -->             <div class="vv">
              <h2>Hz. Mehdi (a.s.) din ahlakını hakim ettiğinde Peygamberimiz (s.a.v.)'in son Peygamber olduğunu kabul etmeyen hiç kimse kalmayacaktır</h2>
             <em>Devamı ...
</em>
             </div>
           </a>
        </li>
        			<li>
                <a href="../20211/Hz._Mehdi_(a.s.html">
<!--                --><!--                -->                <img src="../../../assets/images/blank.gif">
<!--                --><!--                --><!--                -->             <div class="vv">
              <h2>Hz. Mehdi (a.s.) Vehbi ilimlere sahip olacaktır</h2>
             <em>Devamı ...
</em>
             </div>
           </a>
        </li>
        				  </ul>
       <p style="text-align: right;"><a class="dugme" href="../../../list/type/43/name/Ahir%20Zamana%20ait%20Yeni%20Bilgiler.html"> Tüm liste >>
</a></p> 
    </div>
</div>
<div class="clear">&nbsp;</div>


<div class="col2-box4 boxshadow4px">
    <div class="col2-box4" style="padding: 0pt;">
        <h3>İlgili Linkler</h3>
        <ul class="col2-liste">
                                      <li>
                       <a href="../../Makaleler/30708/Allah%27a_kar%c5%9f%c4%b1_derin_bir_sayg%c4%b1_samimi_iman%c4%b1n_%c3%b6nemli_alametlerindendir.html">
                                                                    <img   src='http://g.fmanager.net/Image/objects/6-makaleler/30708_Allaha_karsi_derin_bir_saygi_samimi_imanin_onemli_alametlerindendir.jpg'width='60'   />                                                <div class="vv">
                        <h2>Allah'a karşı derin bir saygı samimi imanın önemli alametlerindendir</h2>
                        <p>Makaleler</p>
                        <em>Devamı ...
</em>
                        </div>
                       </a>
                       </li>
                               <li>
                       <a href="../../Makaleler/27214/Adnan_Oktar%27%c4%b1n_Five_Towns_Jewish_Times_ile_r%c3%b6portaj%c4%b1%2c_4_Haziran_2010%2c_ABD.html">
                                                                    <img   src='http://g.fmanager.net/Image/objects/6-makaleler/27214_Adnan_Oktarin_Five_Towns_Jewish_Times_ile_roportaji4_Haziran_2010_ABD.jpg'width='60'   />                                                <div class="vv">
                        <h2>Adnan Oktar'ın Five Towns Jewish Times ile röportajı, 4 Haziran 2010, ABD</h2>
                        <p>Makaleler</p>
                        <em>Devamı ...
</em>
                        </div>
                       </a>
                       </li>
                               <li>
                       <a href="../../Makaleler/21442/Ateist_ideolojinin_yayg%c4%b1nla%c5%9fmas%c4%b1_d%c3%bcnya_tarihinde_en_%c5%9fiddetli_halini_ahir_zamanda_alacakt%c4%b1r.html">
                                                                    <img   src='http://g.fmanager.net/Image/objects/6-makaleler/21442_ateist_ideolojilerin_yayginlasmasi_dunya_tarihinde_en_siddetli_halini_ahir_zamanda_alacaktir.j.jpg'width='60'   />                                                <div class="vv">
                        <h2>Ateist ideolojinin yaygınlaşması dünya tarihinde en şiddetli halini ahir zamanda alacaktır</h2>
                        <p>Makaleler</p>
                        <em>Devamı ...
</em>
                        </div>
                       </a>
                       </li>
                               <li>
                       <a href="../../Makaleler/21153/Allah%27%c4%b1n_varl%c4%b1%c4%9f%c4%b1_apa%c3%a7%c4%b1k_bir_ger%c3%a7ektir.html">
                                                                    <img   src='http://g.fmanager.net/Image/objects/6-makaleler/21153_Allahin_varligi_apacik_bir_gercektir.jpg'width='60'   />                                                <div class="vv">
                        <h2>Allah'ın varlığı apaçık bir gerçektir</h2>
                        <p>Makaleler</p>
                        <em>Devamı ...
</em>
                        </div>
                       </a>
                       </li>
                               <li>
                       <a href="../../Makaleler/20286/Allah_her_yerdedir.html">
                                                                    <img   src='http://g.fmanager.net/Image/objects/6-makaleler/20286_Allah_her_yerdedir.jpg'width='60'   />                                                <div class="vv">
                        <h2>Allah her yerdedir</h2>
                        <p>Makaleler</p>
                        <em>Devamı ...
</em>
                        </div>
                       </a>
                       </li>
                               <li>
                       <a href="../../Makaleler/19513/Adnan_Oktar%27%c4%b1n_%c4%b0ran_Mehdi_Enstit%c3%bcs%c3%bcnden_temsilcilerle_g%c3%b6r%c3%bc%c5%9fmesinden_foto%c4%9fraflar.html">
                                                                    <img   src='http://g.fmanager.net/Image/objects/6-makaleler/19513_Adnan_Oktarin_Iran_Mehdi_Enstitüsünden_temsilcilerle_gorusmesinden_fotograflar.jpg'width='60'   />                                                <div class="vv">
                        <h2>Adnan Oktar'ın İran Mehdi Enstitüsünden temsilcilerle görüşmesinden fotoğraflar</h2>
                        <p>Makaleler</p>
                        <em>Devamı ...
</em>
                        </div>
                       </a>
                       </li>
                </ul>
    </div>
</div>
<script type="text/javascript">
	function ImgError(source){
    source.src = "../../../assets/images/blank.gif";
    source.onerror = "";
    return true;
}
</script></div><div class="clearpad"></div><div class='banner' id='banners_32076198789550' >



<div class="col2-box1 boxshadow4px">

    <div style="padding: 0pt;" class="col2-box1">
          <h3>İlişkili Videolar</h3>
          <ul class="col2-liste">
          <li><a href='../../Belgesellerden-Secme-Bolumler/24647/Allahin-vadi-kesin-bir-gercektir.html'target='__blank'><img   src='http://g.fmanager.net/Image/objects/60-belgesellerden-secme-bolumler/24647_Allahin_vadi_kesin_bir_gercektir.jpg'width='113'   /><span class='span-overlay'></span><div class='vv'><h2>Allah'ın vadi kesin bir gerçektir</h2><p>Belgesellerden Seçme Bölümler</p></div></a></li><li><a href='../../Belgesellerden-Secme-Bolumler/35929/Hz-Mehdi-(as)nin-mucadele-donemleri.html'target='__blank'><img   src='http://g.fmanager.net/Image/objects/60-belgesellerden-secme-bolumler/35929_Hz_Mehdi_as_nin_mucadele_donemleri.jpg'width='113'   /><span class='span-overlay'></span><div class='vv'><h2>Hz. Mehdi (a.s.)'nin mücadele dönemleri</h2><p>Belgesellerden Seçme Bölümler</p></div></a></li><li><a href='../../Belgesellerden-Secme-Bolumler/24644/Hz-Mehdi-doneminde-bolluk-ve-bereket-olacaktir.html'target='__blank'><img   src='http://g.fmanager.net/Image/objects/60-belgesellerden-secme-bolumler/24644_hz_mehdi_doneminde_bolluk_ve_bereket_olacaktir.jpg'width='113'   /><span class='span-overlay'></span><div class='vv'><h2>Hz. Mehdi döneminde bolluk ve bereket olacaktır</h2><p>Belgesellerden Seçme Bölümler</p></div></a></li><li><a href='../../Belgesellerden-Secme-Bolumler/24638/Hz-Mehdi-baris-ve-sevgiyle-Islam-ahlakini-hakim-kilacaktir.html'target='__blank'><img   src='http://g.fmanager.net/Image/objects/60-belgesellerden-secme-bolumler/24638_hz_mehdi_baris_ve_sevgiyle_islam_ahlakini_hakim_kilacaktir.jpg'width='113'   /><span class='span-overlay'></span><div class='vv'><h2>Hz. Mehdi barış ve sevgiyle İslam ahlakını hakim kılacaktır</h2><p>Belgesellerden Seçme Bölümler</p></div></a></li><li><a href='../../Belgesellerden-Secme-Bolumler/24213/Hz-Isayi-en-guzel-sekilde-karsilamak.html'target='__blank'><img   src='http://g.fmanager.net/Image/objects/60-belgesellerden-secme-bolumler/24213hz_isa_yi_en_guzel_sekilde_karsilamak.jpg'width='113'   /><span class='span-overlay'></span><div class='vv'><h2>Hz. İsa'yı en güzel şekilde karşılamak</h2><p>Belgesellerden Seçme Bölümler</p></div></a></li>          </ul>
      </div>   
    </div>

</div><div class="clearpad"></div>            <!--sag sutun sonu-->
        </div></div>

    <div id="col1"><div style="margin:0 14px;">
        <!--sol sutun baslangic-->

        <!-- AddThis Button BEGIN -->

        <!-- AddThis Button END -->


        
        
        <font size="3" face="Verdana"><strong><font color="#800000">1- Hz. Mehdi (a.s.)&rsquo;dan bahsedilmesi ve Hz. Mehdi (a.s.)'ın gelişinin M&uuml;sl&uuml;manlara m&uuml;jdelenmesi Peygamberimiz (sav)'in bir s&uuml;nnetidir.</font></strong><br />
<br />
Sayın Adnan Oktar sohbetlerinde Mehdiyet konusuna &ouml;zel &ouml;nem vermekte ve bu konuya geniş yer ayırmaktadır. Kuşkusuz ki bu durum, her konuda olduğu gibi Sayın Adnan Oktar&rsquo;ın kendisine Kuran ayetlerini ve Peygamberimiz (sav)'in s&uuml;nnetini rehber edinmesinden kaynaklanmaktadır. <br />
<br />
Allah Kuran'da, İslam ahlakının t&uuml;m yery&uuml;z&uuml;ne hakim olması i&ccedil;in gayret etmenin t&uuml;m M&uuml;slamanların &ouml;nemli bir sorumluluğu olduğunu bildirmiştir. Kuran'da bu konuda &ccedil;ok fazla ayet yer almaktadır. Ve Allah'ın adetullahı gereği, tarihin her d&ouml;neminde hak dinin tebliğinde M&uuml;sl&uuml;manlara &ouml;nderlik eden, onları hidayete y&ouml;nelten manevi bir lider olmuştur. Allah Kuran ayetlerinde t&uuml;m toplumlara, onlara yol g&ouml;sterecek bir el&ccedil;i g&ouml;nderdiğini bildirmiştir. İşte ahir zamanda M&uuml;sl&uuml;manları Kuran ahlakına ve hidayete y&ouml;neltecek, onları birleştirip tek bir &ccedil;atı altında toplayacak olan kişi de Hz. Mehdi (a.s.)&rsquo;dır. Peygamberimiz (sav)'in tevat&uuml;r derecesindeki sahih hadisleriyle bu konu yaklaşık 14 asır &ouml;nce insanlara m&uuml;jdelenmiştir. Peygamberimiz (sav) bu konunun &ouml;nemini hadislerinde &ccedil;ok a&ccedil;ık bir şekilde vurgulamış ve M&uuml;sl&uuml;manların da birbirlerini bu konuyu g&uuml;ndeme getirerek m&uuml;jdelemelerini bildirmiştir:<br />
<br />
</font><blockquote><font size="3" face="Verdana">&ldquo;<strong><u>HZ. MEHDİ İLE M&Uuml;JDELENİN</u>. O Kureyş&rsquo;ten ve Ehl-i Beyt&rsquo;imden bir kişidir.&rdquo;</strong> (Kitab-ul Burhan Fi Alamet-il Ahir zaman, s.13)<br />
</font></blockquote><font size="3" face="Verdana"><br />
</font><blockquote><font size="3" face="Verdana">Bir başka hadisinde ise Peygamberimiz (sav),<strong> &ldquo;Mehdi zuhur eder, <u>HERKES SADECE O&rsquo;NDAN KONUŞUR,</u> O'nun sevgisini i&ccedil;er ve <u>O'NDAN BAŞKA BİR ŞEYDEN BAHSETMEZLER.&rdquo;</u></strong> (Kitab-&uuml;l Burhan Fi Alamet-il Mehdiyy-il Ahir Zaman, s. 33) s&ouml;zleriyle Hz. Mehdi (a.s.)'ın ortaya &ccedil;ıkacağı d&ouml;nemde herkesin bu m&uuml;barek şahıstan bahsedeceğini haber vermiştir.<br />
</font></blockquote><font size="3" face="Verdana"><br />
İşte Sayın Adnan Oktar da, Peygamberimiz (sav)'in bu hadisleri gereği, Hz. Mehdi (a.s.)'ın gelişini M&uuml;sl&uuml;manlara m&uuml;jdelemekte, Hz. Mehdi (a.s.)'ı insanlara tanıtmakta ve Peygamberimiz (sav)'in haber verdiği gibi, konuşmalarında sıklıkla Hz. Mehdi (a.s.)&rsquo;dan bahsetmektedir.<br />
<br />
</font><font size="3" face="Verdana"><img hspace="10" alt="" vspace="5" align="left" width="390" height="405" src="http://g.fmanager.net/Image/gif_resimler/pink_flower.gif" /></font><font size="3" face="Verdana">Peygamberimiz (sav) de yaşadığı d&ouml;nemde, hem beraberindeki M&uuml;sl&uuml;manlara hem de kendisinden sonra yaşayacak nesillere Hz. Mehdi (a.s.)'ı tanıtmış ve konuşmalarında Hz. Mehdi (a.s.)'a geniş yer ayırmıştır. Eğer Mehdiyet &ouml;nemsiz ya da &uuml;zerinde durulmasına gerek olmayan bir konu olsaydı, elbetteki bunun uygulamasını en başta Peygamber Efendimiz (sav)'in konuşmalarında g&ouml;r&uuml;rd&uuml;k. Ancak tam tersine, Peygamberimiz (sav), Allah'ın yol g&ouml;stermesiyle, hem kendisi bu konuyu &ccedil;ok ehemmiyetli g&ouml;rm&uuml;ş hem de M&uuml;sl&uuml;manları, tarihin her d&ouml;neminde bu konuyu g&uuml;ndemde tutmaya, t&uuml;m M&uuml;sl&uuml;man alemini bu konuyla m&uuml;jdelemeye teşvik etmiştir.<br />
<br />
<br />
</font><font color="#800000" size="3" face="Verdana"><strong>2- 1400 yılı aşkın bir s&uuml;redir t&uuml;m b&uuml;y&uuml;k İslam alimleri M&uuml;sl&uuml;manlara Hz. Mehdi (a.s.)'ın gelişini m&uuml;jdelemiş; eserlerinde ve sohbetlerinde bu konuya geniş yer ayırmışlardır.</strong></font><font size="3" face="Verdana"><br />
<br />
Peygamberimiz (sav)'den bu yana, 14 y&uuml;zyıldan beri yaşamış olan t&uuml;m İslam alimleri, Peygamberimiz (sav)'in s&uuml;nneti gereği Mehdiyet konusunun &uuml;zerinde &ouml;nemle durmuş; hadisleri aktarmış ve Hz. Mehdi (a.s.)'ı insanlara tanıtan &ouml;zellikleri t&uuml;m detaylarıyla y&uuml;zlerce sayfa boyunca a&ccedil;ıklamışlardır. Bu kimselerin her biri, İslam tarihinde &ouml;nemli yeri olan, M&uuml;sl&uuml;manlara pek &ccedil;ok konuda yol g&ouml;stermiş, yaşadıkları d&ouml;nemlerin kutbu olmuş b&uuml;y&uuml;k alimlerdir. Yine Ehli S&uuml;nnet&rsquo;in b&uuml;y&uuml;kleri olan hadis imamlarımız, mezhep imamlarımız da Mehdiyet konusuna b&uuml;y&uuml;k &ouml;nem vermiş; eserlerinde Mehdiyet konusunu t&uuml;m detaylarıyla a&ccedil;ıklamışlardır. Hz. Mehdi (a.s.) m&uuml;jdecisi, Hicri 13. yy&rsquo;ın kutbu, b&uuml;y&uuml;k İslam alimi Bedi&uuml;zzaman Said Nursi de eserlerinde y&uuml;zlerce sayfayı Hz. Mehdi (a.s.)'ın gelişine ayırmıştır.<br />
<br />
Mehdiyet konusunu &ouml;nemle vurgulayan ve tarih boyunca M&uuml;sl&uuml;manlara yol g&ouml;sterici olmuş bu b&uuml;y&uuml;k İslam alimlerinden bazılarının isimleri ş&ouml;yledir:<br />
<br />
1. &nbsp;&nbsp; &nbsp; İMAM-I AZAM EBU HANİFE<br />
2. &nbsp;&nbsp; &nbsp; İMAM-I HANBELİ<br />
3. &nbsp;&nbsp; &nbsp; İMAM-I ŞAFİİ<br />
4.&nbsp; &nbsp; &nbsp; İMAM-I MALİKİ<br />
5.&nbsp;&nbsp; &nbsp;&nbsp; İMAM MUHAMMED BİN İSMAİL BUHARİ (BUHARİ)<br />
6.&nbsp; &nbsp; &nbsp; EBUL-H&Uuml;SEYN M&Uuml;SLİM BİN HACCAC KUŞEYRİ (M&Uuml;SLİM)<br />
7. &nbsp;&nbsp; &nbsp; B&Uuml;Y&Uuml;K HADİS ALİMİ&nbsp; MUHAMMED BİN İSA TİRMİZİ<br />
8.&nbsp; &nbsp; &nbsp; HAFIZ EBU DAVUD S&Uuml;LEYMAN BİN EŞ'AS SİCİSTANİ<br />
9.&nbsp;&nbsp; &nbsp;&nbsp; EBU ABDULLAH MUHAMMED BİN YEZİD (İBN-İ MACE)<br />
10.&nbsp;&nbsp;&nbsp; MUHAMMED B. RESUL BERZENCİ <br />
11.&nbsp;&nbsp;&nbsp; ALAEDDİN ALİ B. HİŞAM MUTTAKİ HİNDİ<br />
12.&nbsp;&nbsp;&nbsp; ABD&Uuml;LKADİR GEYLANİ<br />
13.&nbsp;&nbsp;&nbsp; İMAM GAZALİ<br />
14.&nbsp;&nbsp;&nbsp; İMAM-I RABBANİ, <br />
15.&nbsp;&nbsp;&nbsp; MUHYİDDİN ARABİ <br />
16.&nbsp;&nbsp;&nbsp; İBN KESİR<br />
17.&nbsp;&nbsp;&nbsp; İBN TEYMİYE<br />
18.&nbsp;&nbsp;&nbsp; ZAHİDU'L KEVSERİ<br />
19.&nbsp;&nbsp;&nbsp; CELALEDDİN SUYUTİ <br />
20.&nbsp;&nbsp;&nbsp; BEDİ&Uuml;ZZAMAN SAİD NURSİ <br />
21.&nbsp;&nbsp;&nbsp; ŞEHABETTİN İBN-İ HACER ASKELANİ<br />
22.&nbsp;&nbsp;&nbsp; H&Uuml;SEYİN HİLMİ IŞIK<br />
23.&nbsp;&nbsp;&nbsp; MAHMUD ESA'D COŞAN<br />
24.&nbsp;&nbsp;&nbsp; MAHMUT SAMİ RAMAZANOĞLU<br />
25.&nbsp;&nbsp;&nbsp; EBU KASIM TABARANİ<br />
26.&nbsp;&nbsp;&nbsp; ELMALILI HAMDİ YAZIR<br />
27.&nbsp;&nbsp;&nbsp; MUHAMMED B. ALİ ŞEVKANİ <br />
28.&nbsp;&nbsp;&nbsp; MUHAMMED CEMALEDDİN EL-KASİMİ EL-DIMIŞKİ<br />
29.&nbsp;&nbsp;&nbsp; KURTUBİ<br />
30.&nbsp;&nbsp;&nbsp; İMAM MATURİDİ<br />
31.&nbsp;&nbsp;&nbsp; İMAM ACCURİ <br />
32.&nbsp;&nbsp;&nbsp; İBN HAZM<br />
33.&nbsp;&nbsp;&nbsp; PEZDEVİ<br />
34.&nbsp;&nbsp;&nbsp; NESEFİ<br />
35.&nbsp;&nbsp;&nbsp; TEFTAZANİ<br />
36.&nbsp;&nbsp;&nbsp; İBNU'L ARABİ<br />
37.&nbsp;&nbsp;&nbsp; İMAM CAFER ET TAHAVİ<br />
38.&nbsp;&nbsp;&nbsp; BEYAZİ<br />
39.&nbsp;&nbsp;&nbsp; SEYYİD ALUSİ<br />
40.&nbsp;&nbsp;&nbsp; EBU'L M&Uuml;NTEHA<br />
41.&nbsp;&nbsp;&nbsp; ES-SEFFARİNİ<br />
42.&nbsp;&nbsp;&nbsp; ABDULMUHSİN BİN HAMD EL-ABBAD<br />
43.&nbsp;&nbsp;&nbsp; EBU MUHAMMED HASAN B. ALİ EL-BERBEHARİ HANBELİ<br />
44.&nbsp;&nbsp;&nbsp; MUHAMMED NASREDDİN ALBANİ<br />
45.&nbsp;&nbsp;&nbsp; ŞEMSEDDİN MUHAMMED BİN AHMED SEFAREYNİ<br />
46.&nbsp;&nbsp;&nbsp; EBU ABDULLAH MUHAMMED B. CAFER İDRİSİ KETANİ<br />
47.&nbsp;&nbsp;&nbsp; ŞEHABEDDİN AHMED B. MUHAMMED GUMARİ<br />
48.&nbsp;&nbsp;&nbsp; HASANEYN MUHAMMED MAHLUF EL-MISRİ<br />
49.&nbsp;&nbsp;&nbsp; EB'UL-HASAN MUHAMMED B. H&Uuml;SEYİN ABURİ<br />
50.&nbsp;&nbsp;&nbsp; SAİD HAVVA<br />
51.&nbsp;&nbsp;&nbsp; ŞEYH HASAN ADVİ HAMZAVİ <br />
52.&nbsp;&nbsp;&nbsp; M. SIDDIK B. HASAN KUNUCİ<br />
53.&nbsp;&nbsp;&nbsp; MUHAMMED B. HASAN EL-ESNEVİ<br />
54.&nbsp;&nbsp;&nbsp; NUREDDİN ATER<br />
55.&nbsp;&nbsp;&nbsp; EBU ABDULLAH MUHAMMED B. CAFER İDRİSİ KETANİ<br />
56.&nbsp;&nbsp;&nbsp; EBU'S-SADAT MUHAMMED B. MUHAMMED EBU ŞOHBE<br />
57.&nbsp;&nbsp;&nbsp; EN-NEVEVİ<br />
58.&nbsp;&nbsp;&nbsp; EBU'L-FAZL ABDULLAH B. MUHAMMED EL-İDRİSİ<br />
59.&nbsp;&nbsp;&nbsp; MUHAMMED EL-MEKKİ<br />
60.&nbsp;&nbsp;&nbsp; EBU BEKİRAHMED B. MUHAMMED İSKAFİ <br />
61.&nbsp;&nbsp;&nbsp; HAFIZ EBU BEKİR B. HAYSEME <br />
62.&nbsp;&nbsp;&nbsp; EBU-L BEKİR MUHAMMED B. İBRAHİM KELABAZİ BUHARİ <br />
63.&nbsp;&nbsp;&nbsp; EBU KASIM ABDURRAHMAN S&Uuml;HEYLİ<br />
64.&nbsp;&nbsp;&nbsp; YUSUF B. YAHYA MAKDİSİ EŞ-ŞAFİİ <br />
65.&nbsp;&nbsp;&nbsp; ŞEYH İBRAHİM B. MUHAMMED HAMVİNİ <br />
66.&nbsp;&nbsp;&nbsp; İBN-İ HACER EŞ-ŞAFİİ EL-MEKKİ <br />
67.&nbsp;&nbsp;&nbsp; İBN-İ HACER-İ MEKKİ<br />
68.&nbsp;&nbsp;&nbsp; ŞEYH MUHAMMED B. AHMET SEFARİNİ&nbsp; EL-HANBELİ <br />
69.&nbsp;&nbsp;&nbsp; S&Uuml;LEYMAN B. İBRAHİ M KUNDUZİ&nbsp; <br />
70.&nbsp;&nbsp;&nbsp; SEYYİD MUHAMMED SIDDIK KANUCİ BUHARİ <br />
71.&nbsp;&nbsp;&nbsp; EBULFAZL ABDULLAH B. MUHAMMED SIDDIK.<br />
72.&nbsp;&nbsp;&nbsp; ALLAME ŞEVKANİ, <br />
73.&nbsp;&nbsp;&nbsp; HAFIZ ESKALANİ, <br />
74.&nbsp;&nbsp;&nbsp; İBN-İ HACER-İ HEYSEMİ, <br />
75.&nbsp;&nbsp;&nbsp; ŞEBLENCİ, <br />
76.&nbsp;&nbsp;&nbsp; MISIRLI ŞEYH MUHAMMED-İ HANEFİ<br />
77.&nbsp;&nbsp;&nbsp; ŞEYH MUHAMMED SABBAN<br />
78.&nbsp;&nbsp;&nbsp; S&Uuml;VEYDİ<br />
79.&nbsp;&nbsp;&nbsp; AHMED BİN ZEYNİ DEHLAN EŞ ŞAFİİ<br />
80.&nbsp;&nbsp;&nbsp; ABDULVAHHAB ABDULLATİF&nbsp; <br />
81.&nbsp;&nbsp;&nbsp; ALLAME EBU TAYİP<br />
82.&nbsp;&nbsp;&nbsp; SAİD BİN CABİR<br />
83.&nbsp;&nbsp;&nbsp; NİYAZİ MISRİ<br />
84.&nbsp;&nbsp;&nbsp; EN NİFERİ<br />
85.&nbsp;&nbsp;&nbsp; İMAMI AZAM FIKHI EKBER ŞERHİNDE ALİYYUL KAR&rsquo;İ<br />
86.&nbsp;&nbsp;&nbsp; TAHTAVİ<br />
87.&nbsp;&nbsp;&nbsp; ŞEYH MANSUR ALİ NASİF&nbsp; ( TA&Ccedil; İLMİHALİ)<br />
88.&nbsp;&nbsp;&nbsp; SEYYİD KUTUP<br />
89.&nbsp;&nbsp;&nbsp; ŞEYH FAKİH İMANİ<br />
90.&nbsp;&nbsp;&nbsp; ALİ ES SABUNİ&rsquo;<br />
91.&nbsp;&nbsp;&nbsp; MUHAMMED MEHDİ EL-HORASAN,<br />
92.&nbsp;&nbsp;&nbsp; ŞEYH&Uuml;LİSLAM MUSTAFA SABRİ EFENDİ<br />
<br />
&nbsp;<br />
</font><font color="#800000" size="3" face="Verdana"><strong>3- Y&uuml;ce Rabbimiz, Hz. Adem (a.s.)&rsquo;dan ve d&uuml;nyanın yaratılışından bu yana, t&uuml;m d&uuml;nyayı Mehdiyet i&ccedil;in hazırlamıştır.</strong></font><font size="3" face="Verdana"><br />
<br />
</font><blockquote><font size="3" face="Verdana"><strong>Andolsun, Biz Zikir'den sonra Zebur'da da: &quot;Ş&uuml;phesiz Arz'a salih kullarım varis&ccedil;i olacaktır&quot; diye yazdık. (Enbiya Suresi, 105)</strong><br />
</font></blockquote><font size="3" face="Verdana"><br />
D&uuml;nyanın var oluşundan, Hz. Adem (a.s.)&rsquo;ın yaratılışından bu yana Allah t&uuml;m yery&uuml;z&uuml;n&uuml; ve tarihte yaşanan t&uuml;m olayları Mehdiyet i&ccedil;in hazırlamıştır. Hz. İsa (a.s.) zamanında İncil, Hz. Musa (a.s.) d&ouml;neminde ise Tevrat ile, Hz. Davud (as) zamanında Zebur ile t&uuml;m insanlar Hz. Mehdi (a.s.)'ın gelişiyle m&uuml;jdelenmişlerdir. <a href="http://us1.harunyahya.com/Detail/T/7EZU2FZ0164/productId/21284"><em>(İncil&rsquo;de Faraklit ismiyle anlatılan Hz. Mehdi (a.s.) ile ilgili bilgiler</em></a> ve <a href="http://www.harun-yahya.com/imani/hz_ibrahimin_nesli/hz_ibrahim_nesli.html"><em>Tevrat&rsquo;ta Hz. Mehdi (a.s.)&lsquo;ı anlatan s&ouml;zler</em></a>) Allah, Hz. Mehdi (a.s.) ile t&uuml;m yery&uuml;z&uuml;ne İslam ahlakını hakim kılmak i&ccedil;in, Hz. Mehdi (a.s.)'ın gelişinden &ouml;nce d&uuml;nyaya dinsizliği hakim etmiştir. 20. yy&rsquo;da yaşanan t&uuml;m savaşlar; I. ve II. D&uuml;nya Savaşları, yery&uuml;z&uuml;nde h&uuml;k&uuml;m s&uuml;ren ter&ouml;r ve anarşi olayları, M&uuml;sl&uuml;manların yaşadığı bir&ccedil;ok b&ouml;lgede h&uuml;k&uuml;m s&uuml;ren baskılar, zorluk, sıkıntı ve acılar, a&ccedil;lık, sefalet ve kargaşalar, hep Hz. Mehdi (a.s.)&rsquo;ın gelişi &ouml;ncesinde &ouml;zel olarak yaratılmış olaylardır. Allah, Peygamberimiz (sav)'in hadisleriyle Hz. Mehdi (a.s.)'ın &ccedil;ıkışını insanlara haber veren y&uuml;zlerce olayın ger&ccedil;ekleşeceğini bildirmiş; ve bu alametlerin &ccedil;ok b&uuml;y&uuml;k bir b&ouml;l&uuml;m&uuml; de yine bu d&ouml;nemde sırf Hz. Mehdi (a.s.)'ın &ccedil;ıkışı i&ccedil;in &ouml;zel olarak ger&ccedil;ekleştirilmiştir. <em><a href="http://www.hazretimehdi.com/signs/signs_index.html">(Hz. Mehdi (a.s.)'ın &Ccedil;ıkış Alametleri)</a></em> Rabbimiz, Kendi Katına y&uuml;kselttiği Hz. İsa (a.s.)&rsquo;yı, y&uuml;zyıllar sonra b&ouml;yle şerefli bir olay i&ccedil;in tekrar yery&uuml;z&uuml;ne indireceğini bildirmiştir. T&uuml;m bunlar d&uuml;nyanın seyrini değiştiren, d&ouml;n&uuml;m noktası olarak ifade edilen &ccedil;ok b&uuml;y&uuml;k ve tarihi olaylardır. <br />
<br />
Allah'ın, t&uuml;m bu tarihi gelişmeleri, sırf Hz. Mehdi (a.s.)'ın gelişinin hazırlık safhaları olarak yaratmış olması, kuşkusuz ki Mehdiyet konusunun &ouml;nemini ortaya koymaktadır. Buna rağmen Mehdiyet'i ve Hz. Mehdi (a.s.)'ı &ouml;nemsiz g&ouml;rmek ve s&uuml;rekli g&uuml;ndeme getirilmesine gerek olmadığını d&uuml;ş&uuml;nmek son derece hatalı bir bakış a&ccedil;ısıdır. <br />
Allah Peygamberimiz (sav)&rsquo;in bir hadisinde, <strong>&ldquo;d&uuml;nyanın tek bir g&uuml;nl&uuml;k &ouml;mr&uuml; dahi kalmış olsa, Hz. Mehdi (a.s.)'ın gelişi i&ccedil;in bu vakti uzatacağını&rdquo;</strong> bildirmiştir. Sadece bu hadis dahi, Mehdiyet&rsquo;in ne kadar &ouml;nemli olduğunu M&uuml;sl&uuml;manların kavraması a&ccedil;ısından son derece a&ccedil;ıklayıcıdır:<br />
<br />
</font><blockquote><font size="3" face="Verdana">&ldquo;Abdullah (r.a) dan rivayet edilmiştir: Resulullah (s.a.v) buyurdu ki: Ehl-i Beyt&rsquo;imden ismi ismime mutabık olan bir kişi (Hz. Mehdi (a.s.)) başa ge&ccedil;ecektir... <strong>D&Uuml;NYANIN ANCAK BİR G&Uuml;NL&Uuml;K &Ouml;MR&Uuml; KALMIŞ OLSA, ONUN (HZ. MEHDİ (A.S.)'IN) BAŞA GE&Ccedil;MESİ İ&Ccedil;İN CENAB-I ALLAH O G&Uuml;N&Uuml; BEHEMEHAL UZATIR.&rdquo;</strong> (S&uuml;nen-i Tirmizi 4/92)</font><br />
</blockquote><font size="3" face="Verdana"><br />
<br />
<font color="#800000"><strong>4- Kuran'da İslam ahlakının t&uuml;m yery&uuml;z&uuml;ne hakim olacağı vadedilmiştir. Peygamberimiz (sav) de, 14 asırdır hasretle beklenen bu b&uuml;y&uuml;k olaya Hz. Mehdi (a.s.)'ın vesile olacağını bildirmiştir. Mehdiyet&rsquo;ten bahsedilmesini &ouml;nemsiz ve gereksiz g&ouml;rmek, İslam ahlakının hakimiyetini de &ouml;nemsiz g&ouml;rmek olur. </strong></font><br />
<br />
Allah Kuran'ın pek &ccedil;ok ayeti ile, İslam ahlakını t&uuml;m d&uuml;nyada hakim kılacağı bir d&ouml;nem olacağını vadetmiştir. Hadislerde de bu vaadin, Hz. Mehdi (a.s.) vesilesiyle ger&ccedil;ekleşeceği bildirilmiştir. Buna rağmen Mehdiyet&rsquo;in &ouml;nemi yok demek, (Allah&rsquo;ı tenzih ederiz) Kuran'ın da, İslam&rsquo;ın da, İslam ahlakının d&uuml;nya hakimiyetinin de &ouml;nemini takdir edemeyen bir bakış a&ccedil;ısı olduğunu g&ouml;sterir. <br />
<br />
Oysa ki Allah'ın Kuran'da bildirdiği ayetler &ccedil;ok a&ccedil;ıktır. Allah, hak din olan İslam&rsquo;ı d&uuml;nyaya hakim kılacaktır. Allah, her d&ouml;nemde M&uuml;sl&uuml;manları uyarıp korkutan, onları hidayete y&ouml;nelten, onları tek bir birlik altında toplayan bir el&ccedil;i g&ouml;ndermektedir. Bu Allah'ın Adetullahıdır. Ve Allah, Peygamberimiz (sav) ile, ahir zamanda M&uuml;sl&uuml;manların bu manevi liderinin, d&uuml;nyada İslam&rsquo;ın hakimiyetine vesile olacak olan şahsın Hz. Mehdi (a.s.) olduğunu bildirmiştir. İslam alemi, Peygamber Efendimiz (sav)&rsquo;den bu yana 1400 yılı aşkın bir s&uuml;redir Mehdiyet'i aşkla şevkle g&uuml;ndemde tutmuş, Hz. Mehdi (a.s.)'ı sevgiyle anmış ve onun d&ouml;neminde yaşayanlardan olabilmek i&ccedil;in Allah&rsquo;a g&ouml;n&uuml;lden dua etmişlerdir. Hz. Mehdi (a.s.)'ın hemen hemen t&uuml;m geliş alametlerinin ger&ccedil;ekleştiği, i&ccedil;inde bulunduğumuz ahir zamanda ise, elbetteki Mehdiyet t&uuml;m M&uuml;sl&uuml;manların dillerinden d&uuml;ş&uuml;rmemeleri gereken bir konudur. T&uuml;m M&uuml;sl&uuml;manların Mehdiyet konusundan şevkle bahsetmeleri, azim ve gayretle g&uuml;ndemde tutmaları ve t&uuml;m d&uuml;nya M&uuml;sl&uuml;manlarını da, bu y&uuml;zyılda ger&ccedil;ekleşecek olan bu tarihi olayla m&uuml;jdelemeleri son derece &ouml;nemlidir. <br />
<br />
Allah'ın Kuran'da İslam ahlakını yery&uuml;z&uuml;ne hakim kılacağını bildirdiği ve Hz. Mehdi (a.s.)'ın gelişine işaret eden ayetlerden bazıları ş&ouml;yledir:<br />
<br />
</font><blockquote><font size="3" face="Verdana"><strong>Allah i&ccedil;inizden iman edenlere ve salih amelde bulunanlara vaadetmiştir: <u>&ldquo;Hİ&Ccedil; Ş&Uuml;PHESİZ ONLARDAN &Ouml;NCEKİLERİ NASIL 'G&Uuml;&Ccedil; VE İKTİDAR SAHİBİ' KILDIYSA, ONLARI DA YERY&Uuml;Z&Uuml;NDE 'G&Uuml;&Ccedil; VE İKTİDAR SAHİBİ' KILACAK, KENDİLERİ İ&Ccedil;İN SE&Ccedil;İP BEĞENDİĞİ DİNLERİNİ KENDİLERİNE YERLEŞİK KILIP SAĞLAMLAŞTIRACAK VE ONLARI KORKULARINDAN SONRA G&Uuml;VENLİĞE &Ccedil;EVİRECEKTİR...&rdquo;</u> (Nur Suresi, 55)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Ve seveceğiniz bir başka (nimet) daha var: <u>ALLAH'TAN 'YARDIM VE ZAFER (NUSRET)' VE YAKIN BİR FETİH.</u> M&uuml;'minleri m&uuml;jdele. (Saff Suresi, 13)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Andolsun, Biz Zikir&rsquo;den sonra Zebur'da da: <u>&quot;Ş&Uuml;PHESİZ ARZ'A SALİH KULLARIM VARİS&Ccedil;İ OLACAKTIR&quot;</u> diye yazdık. (Enbiya Suresi, 105)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Andolsun, g&ouml;nderilen kullarımıza (şu) s&ouml;z&uuml;m&uuml;z ge&ccedil;miştir: <u>&ldquo;GER&Ccedil;EKTEN ONLAR, MUHAKKAK NUSRET (YARDIM VE ZAFER) BULACAKLARDIR. VE Hİ&Ccedil; Ş&Uuml;PHESİZ; BİZİM ORDULARIMIZ, &Uuml;ST&Uuml;N GELECEK OLANLAR ONLARDIR.&rdquo;</u> (Saffat Suresi, 171-173)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Allah, yazmıştır: <u>&quot;ANDOLSUN, BEN GALİP GELECEĞİM VE EL&Ccedil;İLERİM DE.&quot;</u> Ger&ccedil;ekten Allah, en b&uuml;y&uuml;k kuvvet sahibidir, g&uuml;&ccedil;l&uuml; ve &uuml;st&uuml;n olandır. (M&uuml;cadele Suresi, 21)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>El&ccedil;ilerini hidayet ve hak din &uuml;zere g&ouml;nderen O'dur. <u>&Ouml;YLE Kİ ONU (HAK DİN OLAN İSLAM'I) B&Uuml;T&Uuml;N DİNLERE KARŞI &Uuml;ST&Uuml;N KILACAKTIR</u>; m&uuml;şrikler hoş g&ouml;rmese bile. (Saff Suresi, 9)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong><u>Ki O, EL&Ccedil;İLERİNİ HİDAYETLE VE HAK DİN İLE, DİĞER B&Uuml;T&Uuml;N DİNLERE KARŞI &Uuml;ST&Uuml;N KILMAK İ&Ccedil;İN G&Ouml;NDERDİ.</u> Şahid olarak Allah yeter. (Fetih Suresi, 28)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>M&uuml;şrikler istemese de <u>O DİNİ (İSLAM'I) B&Uuml;T&Uuml;N DİNLERE &Uuml;ST&Uuml;N KILMAK İ&Ccedil;İN </u>el&ccedil;isini hidayetle ve hak dinle g&ouml;nderen O'dur. (Tevbe Suresi, 33)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Ve onlardan sonra <u>SİZİ O ARZA MUTLAKA YERLEŞTİRECEĞİZ. </u>İşte bu, makamımdan korkana ve tehdidimden korkana ait (bir ayrıcalıktır). Fetih istediler, (sonunda) her zorba inat&ccedil;ı bozguna uğrayıp -yok oldu- gitti. (İbrahim Suresi, 14-15)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong><u>ALLAH'IN YARDIMI VE FETİH GELDİĞİ ZAMAN, VE İNSANLARIN ALLAH'IN DİNİNE DALGA DALGA GİRDİKLERİNİ G&Ouml;RD&Uuml;Ğ&Uuml;NDE</u>, hemen Rabbini hamd ile tesbih et ve O'ndan mağfiret dile. &Ccedil;&uuml;nk&uuml; O, tevbeleri &ccedil;ok kabul edendir. (Nasr Suresi, 1-3)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Ş&uuml;phesiz, <u>BİZ SANA APA&Ccedil;IK BİR FETİH VERDİK.</u> &Ouml;yle ki Allah, senin ge&ccedil;miş ve gelecek (her) g&uuml;nahını bağışlasın, &uuml;zerindeki nimetini tamamlasın ve seni dosdoğru bir yola y&ouml;neltsin. Ve Allah, sana '&uuml;st&uuml;n ve onurlu' bir zaferle yardım etsin. (Fetih Suresi, 1-3)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>... Fakat Allah, sizin bilmediğinizi bildi, b&ouml;ylece bundan &ouml;nce <u>SİZE YAKIN BİR FETİH (NASİB) KILDI.</u> (Fetih Suresi, 27)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>... <u>BU YURDUN SONU KİMİNDİR</u>, inkar edenler pek yakında bileceklerdir. (Rad Suresi, 42)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>De ki: <u>&quot;HAK GELDİ, BATIL YOK OLDU. </u>Hi&ccedil; ş&uuml;phesiz batıl yok olucudur.&quot; (İsra Suresi, 81) </strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong><u>KENDİSİNE BEREKETLER KILDIĞIMIZ YERİN DOĞUSUNA DA, BATISINA DA O HOR KILINIP-ZAYIF BIRAKILANLARI (M&Uuml;STAZ'AFLARI) MİRAS&Ccedil;ILAR KILDIK... </u>(Araf Suresi, 137)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Ağızlarıyla Allah'ın nurunu s&ouml;nd&uuml;rmek istiyorlar. <u>OYSA KAFİRLER İSTEMESE DE ALLAH, KENDİ NURUNU TAMAMLAMAKTAN BAŞKASINI İSTEMİYOR.</u> (Tevbe suresi, 32)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Onlar, Allah'ın nurunu ağızlarıyla s&ouml;nd&uuml;rmek istiyorlar. <u>OYSA ALLAH, KENDİ NURUNU TAMAMLAYICIDIR</u>; kafirler hoş g&ouml;rmese bile. (Saff Suresi, 8)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Ve <u>SİZİ ONLARIN TOPRAKLARINA, YURTLARINA, MALLARINA VE DAHA AYAK BASMADIĞINIZ BİR YERE MİRAS&Ccedil;I KILDI.</u> Allah, her şeye g&uuml;&ccedil; yetirendir. (Ahzab Suresi, 27)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Allah, su&ccedil;lu-g&uuml;nahkarlar istemese de,<u> HAKKI (HAK OLARAK) KENDİ KELİMELERİYLE GER&Ccedil;EKLEŞTİRECEKTİR.</u> (Yunus Suresi, 82)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong>Onlar ki, <u>YERY&Uuml;Z&Uuml;NDE KENDİLERİNİ YERLEŞTİRİR, İKTİDAR SAHİBİ KILARSAK</u>, dosdoğru namazı kılarlar, zekatı verirler, ma'rufu emrederler, m&uuml;nkerden sakındırırlar. B&uuml;t&uuml;n işlerin sonu Allah'a aittir. (Hac Suresi, 41)</strong></font><br />
<font size="3" face="Verdana"><strong></strong></font><br />
<font size="3" face="Verdana"><strong><u>GEVŞEMEYİN, &Uuml;Z&Uuml;LMEYİN; EĞER (GER&Ccedil;EKTEN) İMAN ETMİŞSENİZ EN &Uuml;ST&Uuml;N OLAN SİZLERSİNİZ. </u>(Al-i İmran Suresi, 139)</strong></font><br />
</blockquote><font size="3" face="Verdana"><strong><br />
&nbsp;<br />
<font color="#800000"><strong>5- Kuran'da, tarih boyunca k&ouml;t&uuml;lerle iyilerin, deccaliyet ile Mehdiyetin, batıl ile hak dinin bir m&uuml;cadelesi olduğu bildirilmiştir. İşte ahir zamanda deccaliyeti yenecek olan g&uuml;&ccedil; ve hemen her sorunun &ccedil;&ouml;z&uuml;m&uuml; de Mehdiyet olacaktır. </strong></font><br />
<br />
</strong>Allah'ın Kuran ayetlerindeki vaatleri ve Peygamberimiz (sav)'in hadislerinde verilen bilgiler Mehdiyet konusunun &ouml;nemini a&ccedil;ık&ccedil;a ortaya koymaktadır. Kuran'da, tarih boyunca yaşanmış olan her devirde deccallerin ve Mehdilerin m&uuml;cadelesinin var olduğu bildirilmektedir. Ve bu iki fikrin; yani inananlarla inanmayanların, hak ile batılın m&uuml;cadelesi kıyamete kadar da s&uuml;recektir. <br />
&nbsp;<br />
&Ouml;rneğin Hz. İbrahim (a.s.) yaşadığı d&ouml;nemin Mehdisi, Nemrud da o d&ouml;nemin deccali olmuştur. Hz. Musa (a.s.) da kendi d&ouml;neminin Mehdisi ve Firavun da o zamanın deccali olmuştur. Tarihin her aşamasında, deccallerin ve Mehdilerin g&ouml;ğ&uuml;s g&ouml;ğ&uuml;se &ccedil;ok g&uuml;&ccedil;l&uuml; bir m&uuml;cadelesi vardır.<br />
<br />
Peygamberimiz (sav), ahir zamanda &ccedil;ıkacak olan deccalin, bu g&uuml;ne kadar gelmiş ge&ccedil;miş en azılı, en etkili ve en g&uuml;&ccedil;l&uuml; Deccal hareketi olacağını bildirmiştir. İşte buna karşılık Allah, Ahir Zaman Mehdisi&rsquo;ni de o denli &ouml;nemli kılmış, Hz. Mehdi (a.s.)'ın deccal ile olan m&uuml;cadelesini ve bunun sonucundaki galibiyetini de o denli b&uuml;y&uuml;k bir olay olarak yaratmıştır. <br />
<br />
Bedi&uuml;zzaman Said Nursi Hazretleri risalelerinde, Ahir Zaman Mehdisi&rsquo;nin &ccedil;ıkacağı i&ccedil;erisinde bulunduğumuz bu zamanı, <strong>&ldquo;ahir zamanın EN B&Uuml;Y&Uuml;K FESADI d&ouml;nemi&rdquo;</strong> <em>(Mektubat, s. 411 - 412) </em>s&ouml;zleriyle tanımlamıştır. Ger&ccedil;ekten de i&ccedil;erisinde bulunduğumuz bu d&ouml;nemde, d&uuml;nya tarihinin gelmiş ge&ccedil;miş en b&uuml;y&uuml;k fesadı yaşanmaktadır. Hz. Adem (a.s.)&rsquo;dan bu yana, bu devre kadar bu derece b&uuml;y&uuml;k bir fesat ne Nemrud ne Firavun ne de H&uuml;lag&uuml; devrinde hi&ccedil; olmamıştır. Bu kadar b&uuml;y&uuml;k bir fesat ilk defa yaşanmaktadır. Ahir zaman deccali artık &ccedil;ıkmıştır. Deccal d&uuml;nya tarihinin gelmiş ge&ccedil;miş en b&uuml;y&uuml;k fesadıdır. &Ouml;yle ki tarih boyunca b&uuml;t&uuml;n peygamberler, ahir zaman deccalinin şerrinden Allah&rsquo;a sığınmışlardır. Halihazırda tarih boyunca yaşamış olan t&uuml;m Firavunların, Nemrudların ve deccallerin toplamından daha şiddetli bir k&uuml;f&uuml;r d&uuml;nyaya hakim olmuştur. Dolayısıyla elbette ki, ahir zamanda gelecek olan Hz. Mehdi (a.s.) da bu denli &ouml;nemli bir şahıs ve Mehdiyet de bu denli &ouml;nemle &uuml;zerinde durulması gereken bir konudur. <br />
<br />
Tarihte ilk defa bu kadar b&uuml;y&uuml;k bir Deccal ve bu kadar şiddetli bir k&uuml;f&uuml;r hakim iken, Mehdiyet'in &ouml;nemsizliğinden bahsetmek ve Hz. Mehdi (a.s.)'ın gelmeyeceğini iddia etmek, Allah'ın Kuran ile bildirdiği Adetullahına da uygun değildir. Rabbimiz d&uuml;nya tarihi boyunca her zaman b&ouml;yle bir Adetullah yaratmıştır; her zaman k&uuml;f&uuml;r olmuştur, deccaller olmuştur ve bunların karşısında da mehdiler olmuştur. Ama bu d&ouml;nemde en b&uuml;y&uuml;k deccaller en b&uuml;y&uuml;k nemrudlar en b&uuml;y&uuml;k firavunlar ortaya &ccedil;ıkmışken, bu d&ouml;nemde &ldquo;Hz. Mehdi (a.s.) yoktur, gelmeyecektir&rdquo; denmesi Kuran'ın mantığına uygun değildir. Deccal varsa mutlaka Hz. Mehdi (a.s.) da vardır. Dolayısıyla da b&ouml;yle bir d&ouml;nemde Hz. Mehdi (a.s.)&rsquo;dan bahsetmemek olmaz. &Ccedil;&uuml;nk&uuml; &ccedil;&ouml;z&uuml;m Mehdiyet&rsquo;tedir. Hz. Mehdi (a.s.) vesilesiyle M&uuml;sl&uuml;manların yaşadığı t&uuml;m sıkıntılar, acılar, zorluklar son bulacak; yery&uuml;z&uuml;nde h&uuml;k&uuml;m s&uuml;ren dinsizlik yerle bir olacak, nemrudlar, firavunlar, deccaller ancak Hz. Mehdi (a.s.) vesilesiyle mağlup olacaklardır. <br />
<strong><br />
&nbsp; <font color="#800000"><strong><br />
6- Kimi insanların Hz. Mehdi (a.s.)'ın gelmesini istememelerinin altında d&uuml;nyaya y&ouml;nelik menfaat kaygısı yatmaktadır.</strong></font><br />
<br />
</strong>Kimi insanların Mehdiyet konusuna hep ş&uuml;phe ve teredd&uuml;t ile yaklaşmalarının, Hz. Mehdi (a.s.)'ın geleceğini bildiren apa&ccedil;ık delillere dahi s&uuml;rekli olarak muhalefet etmelerinin altındaki en &ouml;nemli neden <strong>&ldquo;menfaat kaygısı&rdquo;</strong>dır. Hz. Mehdi (a.s.)'ın gelişi ve Mehdiyet makamı t&uuml;m inananlar i&ccedil;in bir rahmet olduğu; O&rsquo;nun vesilesiyle t&uuml;m İslam alemine barış, adalet, bolluk, bereket, huzur, refah, mutluluk geleceği halde, yine de bazı kimseler Hz. Mehdi (a.s.)'ın gelmesini istememektedirler.<br />
<br />
Elbetteki bu son derece dikkat &ccedil;ekici ve d&uuml;ş&uuml;nd&uuml;r&uuml;c&uuml; bir durumdur. Bir M&uuml;sl&uuml;manın, d&uuml;nyadaki t&uuml;m M&uuml;sl&uuml;manların iyiliğini, rahatını, birlik ve beraberliğini, g&uuml;venlik ve refah i&ccedil;erisinde yaşamalarını istemesi gerekir. Ama buna rağmen bazı kimseleri bu duruma vesile olacak kişinin gelmesinden yana değillerdir. Bunun altında yatan sebepler kuşkusuz rahmani değil, nefsanidir. Kimileri sakin, kendi halinde, d&uuml;zenli, tertipli bir hayat yaşamak i&ccedil;in; kimileri kurulu d&uuml;zenlerinin bozulmaması, olay &ccedil;ıkmaması, işlerine, evliliklerine, sosyal hayatlarına zarar gelmemesi i&ccedil;in; kimileri mehdilik vasfını kendi bağlı oldukları hocalarına atfedebilmek ve b&ouml;ylecekendilerince bu kimsenin konumunu, itibarını korumak amacıyla Hz. Mehdi (a.s.)'ın gelmesini istememekte ve Mehdiyet konusundan bahsedilmesinden de rahatsız olmaktadırlar. Ayrıca eğer Hz. Mehdi (a.s.) gelecek olursa, O&rsquo;na destek olmaları, onunla birlikte bir&ccedil;ok fedakarlığı, zorluk ve sıkıntıyı g&ouml;ze almaları; gerektiğinde aile ve işlerine y&ouml;nelik &ccedil;ıkarlarından feragat etmeleri; geleceğe y&ouml;nelik d&uuml;nyevi planlarından vazge&ccedil;meleri gerekebileceğini d&uuml;ş&uuml;nerek, Hz. Mehdi (a.s.) konusunun g&uuml;ndeme gelmesinden ka&ccedil;ınmaktadırlar. <br />
<br />
Ancak bilinmelidir ki bu kimselerin i&ccedil;erisinde bulunduğu durum, tarihin her d&ouml;neminde ve Peygamberimiz (sav) zamanında da yaşanmıştır. Her M&uuml;sl&uuml;man toplumunda, iman şevki az olan, İslam'ın menfaatlerindense kendi menfaatlerini &ouml;nde tutan ve d&uuml;nya hayatının &ccedil;ekiciliğine kapılan insanlar olmuştur. Kuran'da, Peygamberimiz (sav) M&uuml;sl&uuml;manları hicret etmeye, m&uuml;cadeleye ya da savaşa &ccedil;ağırdığında, ailesini, işini, &ccedil;ocuklarını bahane ederek geride kalan insanların durumunu anlatan pek &ccedil;ok ayet vardır. Bu kimseler Peygamber (sav) ile birlikte Allah i&ccedil;in zorluklara, fedakarlıklara katlanmayı adeta bir kabus gibi g&ouml;rm&uuml;ş ve itinayla uzak durmuşlardır. <br />
<br />
İşte ahir zamanda Hz. Mehdi (a.s.)'ın gelmesini, Hz. Mehdi (a.s.)'dan bahsedilmesini istemeyen kimselerin durumu da bundan farklı değildir. Samimi M&uuml;sl&uuml;manlara Hz. Mehdi (a.s.)'ın gelişi bir nur gibi gelirken, d&uuml;nya hayatını hedefleyen insanlar bu durumdan şiddetle &ccedil;ekinmektedirler. Bir ayette Allah bu kimselerin durumunu ş&ouml;yle a&ccedil;ıklamaktadır:<br />
<strong><u><br />
</u></strong></font><blockquote><font size="3" face="Verdana"><strong><u>Eğer yakın bir yarar ve orta bir sefer olsaydı, onlar mutlaka seni izlerlerdi. Ama zorluk onlara uzak geldi. </u>&quot;Eğer g&uuml;&ccedil; yetirseydik muhakkak seninle birlikte&nbsp; (savaşa) &ccedil;ıkardık.&quot; diye sana Allah adına yemin edecekler. Kendi nefislerini helaka s&uuml;r&uuml;kl&uuml;yorlar. Allah onların ger&ccedil;ekten yalan s&ouml;ylediklerini biliyor. (Tevbe Suresi, 42)</strong></font><br />
</blockquote><font size="3" face="Verdana"><strong><br />
<br />
<font color="#800000"><strong>7 &ndash; Hz. Mehdi (a.s.)'ın gelişi Allah'ın yarattığı bir kaderdir. Aleyhte y&uuml;r&uuml;t&uuml;len hi&ccedil;bir &ccedil;aba, bu m&uuml;jdenin ger&ccedil;ekleşmesine engel olamayacaktır.</strong></font><u><br />
<br />
</u></strong>Allah bir ayetinde,<strong> &ldquo;El&ccedil;ilerini hidayet ve hak din &uuml;zere g&ouml;nderen O'dur. &Ouml;yle ki onu (hak din olan İslam'ı) b&uuml;t&uuml;n dinlere karşı &uuml;st&uuml;n kılacaktır; <u>M&Uuml;ŞRİKLER HOŞ G&Ouml;RMESE BİLE.</u>&rdquo; </strong>(Saff Suresi, 9) şeklinde buyurmuştur. Bu ayet bize &ccedil;ok &ouml;nemli bir ger&ccedil;eği haber vermektedir. Ne deccallerin, nemrutların, firavunların m&uuml;cadelesi ne de kalplerinde hastalık ya da imanlarında zayıflık bulunan kimselerin &ccedil;abaları Allah'ın Adetullahına asla etki edemez. Allah her ne olursa olsun, vaadini yerine getirendir. Allah, Peygamber Efendimiz (sav)&rsquo;in hadisleri ile Hz. Mehdi (a.s.)'ın bu y&uuml;zyılda geleceğini ve İslam alemini şereflendireceğini haber vermiştir. Allah vaadini ger&ccedil;ekleştirecek ve inşaAllah Hz. Mehdi (a.s.) t&uuml;m M&uuml;sl&uuml;manların manevi lideri olarak yery&uuml;z&uuml;ne Kuran ahlakını hakim kılacaktır.</font>
                        <strong>2010-03-07 04:34:10</strong>




        <br/>
        <div style="text-align:right"><a class="dugme" href="../../../list/type/43/name/Ahir%20Zamana%20ait%20Yeni%20Bilgiler.html"> Ahir Zamana ait Yeni Bilgiler &rArr;</a></div>
        <!--sol sutun sonu-->
    </div></div>
</div>
﻿		<div class="clear"></div>
</div>

</div>
</div>

<!--BOTTOM-->
<div id="bottom">
    <div class="center">

<div class="doublecolumn-wrp">
      <div id="main-bot"><div id="bg04"><div id="bg05"><div id="bg06"></div></div></div></div>
      <div id="main-bot2col"><div id="bg04"><div id="bg05"><div id="bg06"></div></div></div></div>
</div>    

      <div id="bottomcontent">
        <!--ALT BANT içerik-->

        <div class="botcols3">
                      </div>

        <!--ALT BANT içerik sonu-->
      </div>

    </div>
</div>


<!--FOOTER-->  
<div class="footer1"><div class="center">
    <a href='../../../list/type/14/name/Harun%20Yahya%20Etkiler/index.html'>Harun Yahya Etkiler</a> | <a href='../../../list/type/22/name/Bas%c4%b1nda%20Harun%20Yahya/index.html'>Basında Harun Yahya</a> | <a href='../../../list/type/7/name/Sunumlar/index.html'>Sunumlar</a> | <a href='../../../list/type/3/name/Ses%20kasetleri/index.html'>Ses kasetleri</a> | <a href='../../../list/type/15/name/%c4%b0nteraktif%20CD.html'ler/'>İnteraktif CD'ler</a> | <a href='../../../list/type/9/name/Konferans%20setleri/index.html'>Konferans setleri</a> | <a href='../../../list/type/4/name/Radyo%20program%c4%b1/%20Piyesler/index.html'>Radyo programı / Piyesler</a> | <a href='../../../list/type/13/name/Bro%c5%9f%c3%bcrler/index.html'>Broşürler</a>| <a href="http://harunyahya.info/bilgi/sitehakkinda">Site Hakkında</a> | <a href="http://tr.harunyahya.net/" title="Harun Yahya, Adnan Oktar" rel="muse" target="_blank">HarunYahya.net</a> | <a href="javascript:setHomepage()">Ana sayfanız yapın</a> | <a href="javascript:addfav()">Sık kullanılanlara ekle</a> | <a href="../../../bilgi/rss.html"> RSS Servisi</a></div></div>

<div class="footer2"><div class="center">
    Bu sitede yayınlanan tüm materyaller, Sayın Adnan Oktar’ı referans göstermek koşuluyla telif hakkı ödemeksizin kopyalanabilir ve çoğaltılabilir</br>© Sitemizde ve diğer tüm Harun Yahya eserlerinde yer alan Sayın Adnan Oktar’a ait şahsi fotoğrafların bütün yayın hakları Global Yayıncılık Ltd.Şti’ne aittir. Kısmen de olsa izinsiz kullanılamaz ve yayınlanamaz.</br>© 1994 Harun Yahya. www.harunyahya.org</div></div>
<!--FOOTER SONU-->

<a href="#bg" id="toTop">page_top</a>
<script type="text/javascript" src="../../../assets/js/bundle_0e1537593b0c04babea45c13ce1f72fc090d.js?1593035269"></script>
<script type="text/javascript">
var object_name = "Sayin-Adnan-Oktarin-konusmalarinda-Islam-ahlakinin-hakimiyeti-ve-Mehdiyet-konularina-ozel-yer-ayirmasi-Kuran-ahlakinin-bir-geregi-ve-Peygamberimiz-(sav)in-bir-sunnetidir";
var object_language = "Turkce";
var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7862096-15']);
  _gaq.push(['_setDomainName', 'harunyahya.info']);
  _gaq.push(['_setCustomVar',
             1,                   // This custom var is set to slot #1.  Required parameter.
             "Eser_Adi",           // The top-level name for your online content categories.  Required parameter.
             object_name,  
             1                    // Sets the scope to page-level.  Optional parameter.
          ]);
  _gaq.push(['_setCustomVar',
             2,                   
             "Eser_Turu",         
             "Ahir Zamana ait Yeni Bilgiler", 
             1 
          ]);
  _gaq.push(['_setCustomVar',
             4,                   // This custom var is set to slot #1.  Required parameter.
             "Eser_Dili",           // The top-level name for your online content categories.  Required parameter.
             object_language,  
             1                    // Sets the scope to page-level.  Optional parameter.
          ]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  <!--  -->
</script>

        <div id="selected_share" style="position: absolute; display: none;">
            <a id="sTwitterBtn" target="_blank" href="#" style="text-decoration: none">
                <img src="../../../assets/images/m_twitter.png" />
            </a>
            <a id="sMailBtn" href="#" style="text-decoration: none">
                <img src="../../../assets/images/m_mail.png" />
            </a>
        </div>



<!-- Call for AddThis init() function -->
<script>

    function initAddThis()
     {
          addthis.init()
     }
     initAddThis();


    current_url = "Say%c4%b1n_Adnan_Oktar%27%c4%b1n%2c_konu%c5%9fmalar%c4%b1nda_%c4%b0slam_ahlak%c4%b1n%c4%b1n_hakimiyeti_ve_Mehdiyet_konular%c4%b1na_%c3%b6zel_yer_ay%c4%b1rmas%c4%b1%2c_Kuran_ahlak%c4%b1n%c4%b1n_bir_gere%c4%9fi_ve_Peygamberimiz_(sav)%27in_bir_s%c3%bcnnetidir.d607.d";

    lst_text = '';

    $("*").click(function(){
        var text=getSelectedText();

        if (text.length < 10){
           $("#selected_share").fadeOut();
        }
    });

    $("p, span").mouseup(function(event) {
        var text=getSelectedText();

        if (text != '' && lst_text != text) {

            if(text.length > 115){
                t_text  = text.trim().substr(0, 114) + "..";
            }else{
                t_text  = text.trim();
            }
            // p_top = Math.ceil($(this).position().top);
            //console.log($(this).position().left);

            //console.log(text);

            var r=window.getSelection().getRangeAt(0).getBoundingClientRect();
            var relative=document.body.parentNode.getBoundingClientRect();

            r_top = (r.bottom -relative.top)+5;

            document.getElementById("selected_share").style.top = r_top+"px";
            document.getElementById("selected_share").style.left = $(this).position().left+"px";

            var sUrl = "https://twitter.com/intent/tweet?text="+encodeURIComponent(t_text)+ "&url="+current_url;
            var mUrl = "mailto:?subject="+document.title+"&body="+encodeURIComponent(text+" -> ")+current_url;

            $("#sTwitterBtn").attr("href", sUrl);
            $("#sMailBtn").attr("href", mUrl);

            $("#selected_share").fadeIn();

            lst_text = text;
        }
    });

    function getSelectedText() {
        if (window.getSelection) {
            return window.getSelection().toString();
        } else if (document.selection) {
            return document.selection.createRange().text;
        }
        return '';
    }

</script>


<style>
#templatemo_footer {
   z-index: 100;
   background-color: #D00000;
   text-align: center;
   position:fixed;
   bottom:0;
   left:0;
   width:100%;
   height:35px;
 }
</style>	

</body>

<!-- Mirrored from allahsevgisi.net/tr/Ahir-Zamana-ait-Yeni-Bilgiler/21432/Sayın_Adnan_Oktar%27ın,_konuşmalarında_İslam_ahlakının_hakimiyeti_ve_Mehdiyet_konularına_özel_yer_ayırması,_Kuran_ahlakının_bir_gereği_ve_Peygamberimiz_(sav)%27in_bir_sünnetidir by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Jul 2021 21:58:22 GMT -->
</html>
